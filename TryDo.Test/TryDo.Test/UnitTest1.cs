﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TryDo.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void SuccessTest()
        {
            var d = new TryDo().Do(true).Do(true).Get();

            Assert.IsTrue(d.IsSucces);
        }
        [TestMethod]
        public void UnSuccessTest()
        {
            var d = new TryDo().Do(false).Do(true).Get();

            Assert.IsFalse(d.IsSucces);
        }
    }

    class TryDo
    {
        private Result _get;

        public TryDo Do(bool condition)
        {
            if (condition)
            {
                _get = Result.Success();
                return this;
            }
            else
            {
                _get = Result.Failed();
                return this;
            }
        }

        public Result Get()
        {
            return _get;
        }
    }

    public class Result
    {
        private readonly bool _succes;

        private Result(bool succes)
        {
            _succes = succes;
        }

        public bool IsSucces
        {
            get { return _succes; }
        }

        public static Result Success()
        {
            return new Result(true);
        }

        public static Result Failed()
        {
            return new Result(false);
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFinancePlanner.MongoDb.Data;

namespace MyFinancePlanner.MongoDb.Test
{
    [TestClass]
    public class MongoDbFilterTest
    {
        [TestMethod]
        public void FilterTest()
        {

            var filter = MongoDbFilter<Person>.Create()
                .Where(x => x.Name == "Walter");
        }
    }

    public class Person
    {
        public string Name { get; set; }
    }
}

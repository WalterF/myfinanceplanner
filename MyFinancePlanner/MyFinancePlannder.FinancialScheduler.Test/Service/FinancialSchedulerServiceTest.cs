﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Domain.Mapper;
using MyFinancePlanner.FinancialScheduler.Service;

namespace MyFinancePlanner.FinancialScheduler.Test.Service
{
    [TestClass]
    public class FinancialSchedulerServiceTest
    {
        [TestInitialize]
        public void Init()
        {
            StartUp.ObjectResolver.SetUp();
            MyFinancePlanner.LedgerEntries.StartUp.ObjectResolver.SetUp();
        }

        [TestMethod, TestCategory("Create Test Data"), Ignore()]
        public void CreateFinancialYearTest()
        {
            var service = new FinancialSchedulerService();
            var result = service.Create("Year 2016", 200m, new DateTime(2016, 1, 1), new DateTime(2016, 12, 31));

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Groups.Any());
        }

        [TestMethod, TestCategory("Create Test Data")]
        public void CorrectGroupId()
        {
            var service = new FinancialSchedulerService();
            var result = service.GetPayments(new Id("aa4c145e-86a9-476e-9497-8b4094fa1e5e"));

            foreach (var group in result.Groups)
            {
                if (group.Id == Guid.Empty)
                {
                    group.Id = new Id();
                }
            }

            var mapper = new FinancialYearMapper();
            var data = mapper.MapToData(result);

            Assert.AreEqual(result.Groups.First().Id, data.Groups.First()._Id);
            /*
            var gateway = new FinancialYearRepository(new FinancialYearGateway(), new FinancialYearMapper());
            gateway.Save(result);
            */
        }
    }
}

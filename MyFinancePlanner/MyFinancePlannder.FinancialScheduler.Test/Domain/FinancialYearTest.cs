﻿using System;
using MyFinancePlanner;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Domain;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.FinancialScheduler.Test.Domain
{
    [TestClass]
    public class FinancialYearTest
    {
        [TestMethod, TestCategory("FinancialYear")]
        public void GetBalance_test_use_balance()
        {
            var financialYear = new FinancialYear
            {
                EstimateStartBalance = 800,
                Balance = 850
            };

            Assert.AreEqual(850m, financialYear.GetCurrentBalance());
        }
        [TestMethod, TestCategory("FinancialYear")]
        public void GetBalance_test_use_estimateStartBalance()
        {
            var financialYear = new FinancialYear
            {
                EstimateStartBalance = 800,
                Balance = 0
            };

            Assert.AreEqual(800m, financialYear.GetCurrentBalance());
        }

        [TestMethod, TestCategory("FinancialYear")]
        public void GetBalance_test_use_calculate_income()
        {
            var financialYear = new FinancialYear
            {
                EstimateStartBalance = 800,
                Balance = 1000
            };
            var income = financialYear.CreateGroup("inkomen", GroupTypeEnum.Income, new Id());
            income.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 1000, ProcessingDate = DateTimeTester.Date.AddMonths(-1)});
            income.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 1000, ProcessingDate = null });

            Assert.AreEqual(2000m, financialYear.GetCurrentBalance());
        }

        [TestMethod, TestCategory("FinancialYear")]
        public void GetBalance_test_use_calculate_cost()
        {
            var financialYear = new FinancialYear
            {
                EstimateStartBalance = 800,
                Balance = 1000
            };
            var costGroup = financialYear.CreateGroup("Uitgaven", GroupTypeEnum.Cost, new Id());
            costGroup.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 500, ProcessingDate = DateTimeTester.Date.AddMonths(-1) });
            costGroup.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 500, ProcessingDate = null });

            Assert.AreEqual(500m, financialYear.GetCurrentBalance());
        }

        [TestMethod, TestCategory("FinancialYear")]
        public void GetBalance_test_use_calculate_current_balance()
        {
            var financialYear = new FinancialYear
            {
                EstimateStartBalance = 800,
                Balance = 1000
            };
            var income = financialYear.CreateGroup("inkomen", GroupTypeEnum.Income, new Id());
            income.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 1000, ProcessingDate = DateTimeTester.Date.AddMonths(-1) });
            income.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 1000, ProcessingDate = null });

            var costGroup = financialYear.CreateGroup("Uitgaven", GroupTypeEnum.Cost, new Id());
            costGroup.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 500, ProcessingDate = DateTimeTester.Date.AddMonths(-1) });
            costGroup.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 500, ProcessingDate = null });

            Assert.AreEqual(1500m, financialYear.GetCurrentBalance());
        }

        #region Estimates
        [TestMethod, TestCategory("FinancialYear")]
        public void GetEstimatedBalance_test_use_calculate_income()
        {
            var financialYear = new FinancialYear
            {
                EstimateStartBalance = 800,
                Balance = 1000
            };
            var income = financialYear.CreateGroup("inkomen", GroupTypeEnum.Income, new Id());
            income.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 1000, ProcessingDate = DateTimeTester.Date.AddMonths(-1) });
            income.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 1000, ProcessingDate = null });

            Assert.AreEqual(3000m, financialYear.GetEstimatedBalance());
        }

        [TestMethod, TestCategory("FinancialYear")]
        public void GetEstimatedBalance_test_use_calculate_cost()
        {
            var financialYear = new FinancialYear
            {
                EstimateStartBalance = 800,
                Balance = 1000
            };
            var costGroup = financialYear.CreateGroup("Uitgaven", GroupTypeEnum.Cost, new Id());
            costGroup.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 500, ProcessingDate = DateTimeTester.Date.AddMonths(-1) });
            costGroup.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 500, ProcessingDate = null });

            Assert.AreEqual(0m, financialYear.GetEstimatedBalance());
        }

        [TestMethod, TestCategory("FinancialYear")]
        public void GetEstimatedBalance_test_use_calculate_current_balance()
        {
            var financialYear = new FinancialYear
            {
                EstimateStartBalance = 800,
                Balance = 1000
            };
            var income = financialYear.CreateGroup("inkomen", GroupTypeEnum.Income, new Id());
            income.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 1000, ProcessingDate = DateTimeTester.Date.AddMonths(-1) });
            income.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 1000, ProcessingDate = null });

            var costGroup = financialYear.CreateGroup("Uitgaven", GroupTypeEnum.Cost, new Id());
            costGroup.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 500, ProcessingDate = DateTimeTester.Date.AddMonths(-1) });
            costGroup.AddContract(new FinancialScheduler.Domain.Contract() { Cost = 500, ProcessingDate = null });

            Assert.AreEqual(2000m, financialYear.GetEstimatedBalance());
        }
    }
        #endregion

    public static class DateTimeTester
    {
        public static DateTime Date {
            get { return new DateTime(2016, 3, 15); }
        }
    }
}

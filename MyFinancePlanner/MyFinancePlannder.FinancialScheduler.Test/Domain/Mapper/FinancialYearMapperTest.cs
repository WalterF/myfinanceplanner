﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Data;
using MyFinancePlanner.FinancialScheduler.Domain.Factory;
using MyFinancePlanner.FinancialScheduler.Domain.Mapper;
using MyFinancePlanner.FinancialScheduler.StartUp;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.FinancialScheduler.Test.Domain.Mapper
{
    [TestClass]
    public class FinancialYearMapperTest
    {
        [TestInitialize]
        public void Init()
        {
            ObjectResolver.SetUp();
        }
        [TestMethod]
        public void ModelToDataMapper()
        {
            var financialYear = FinancialYearFactory.Create("test year", 800, new DateTime(2016, 1, 1),
                new DateTime(2016, 12, 31));
            var group = financialYear.CreateGroup("test group", GroupTypeEnum.Cost, new Id());
            var account = ContractFactory.Create("Contract Name", 35, new DateTime(2016, 1, 5));
            financialYear.Groups[0].AddContract(account);
            
            var mapper = new FinancialYearMapper();
            var result = mapper.MapToData(financialYear);

            Assert.IsNotNull(result);
            Assert.AreEqual(financialYear.Id, result._Id);
            Assert.AreEqual(financialYear.Title, result.Title);
            Assert.AreEqual(financialYear.StartDate, result.StartDate);
            Assert.AreEqual(financialYear.EndDate, result.EndDate);
            Assert.AreEqual(financialYear.Balance, result.Balance);

            var actualGroup = result.Groups.FirstOrDefault();
            Assert.IsNotNull(actualGroup);
            Assert.AreEqual(group.Id, actualGroup._Id);
            Assert.AreEqual(group.GroupName, actualGroup.GroupName);
            Assert.AreEqual((int)group.GroupType, actualGroup.GroupType);
            Assert.AreEqual(group.LedgerEntryGroupGuid, actualGroup.GroupGuid);

            var accountData = result.Groups[0].Accounts.First();
            Assert.IsNotNull(accountData);
            Assert.AreEqual(account.Id, accountData._Id);
            Assert.AreEqual(account.ContractName, accountData.Description);
            Assert.AreEqual(account.Cost, accountData.Cost);
            Assert.AreEqual(account.InterestDate, accountData.InterestDate);
            Assert.AreEqual(account.ExpectedDate, accountData.ExpectedDate);
            Assert.AreEqual(account.ProcessingDate, accountData.ProcessingDate);
        }

        [TestMethod]
        public void DataToModelMapper()
        {
            var financialYear = new FinancialYearData
            {
                _Id = Guid.NewGuid(),
                Balance = 900m,
                EstimateStartBalance = 850m,
                StartDate = new DateTime(2016, 3, 1),
                EndDate = new DateTime(2016, 3, 31),
                Groups = new Collection<GroupData>()
                {
                    new GroupData()
                    {
                        _Id = new Guid("00000002-505B-44E7-B8CA-6ACAC1646CA3"),
                        GroupName = "Groupname",
                        GroupType = 1, 
                        GroupGuid = new Guid("00000003-505B-44E7-B8CA-6ACAC1646CA3"), 
                        Accounts = new Collection<AccountsData>()
                        {
                            new AccountsData()
                            {
                                 _Id = new Guid(), Description="Account data", 
                                 ContractGuid = new Guid(),
                                 Cost=800m, Name="Account name", 
                                 ExpectedDate=DateTime.Now, 
                                 InterestDate=DateTime.Now.AddDays(1),
                                 ProcessingDate=DateTime.Now.AddDays(2)
                            }
                        }
                    }
                }
            };

            var mapper = new FinancialYearMapper();
            var result = mapper.MapToModel(financialYear);

            Assert.IsNotNull(result);
            Assert.AreEqual(financialYear._Id, result.Id);
            Assert.AreEqual(financialYear.Title, result.Title);
            Assert.AreEqual(financialYear.StartDate, result.StartDate);
            Assert.AreEqual(financialYear.EndDate, result.EndDate);
            Assert.AreEqual(financialYear.Balance, result.Balance);

            var actualGroup = result.Groups.First();
            var groupData = financialYear.Groups.First();
            Assert.IsNotNull(actualGroup);
            Assert.AreEqual(groupData._Id, actualGroup.Id);
            Assert.AreEqual(groupData.GroupName, actualGroup.GroupName);
            Assert.AreEqual(groupData.GroupType, (int)actualGroup.GroupType);
            Assert.AreEqual(groupData.GroupGuid, actualGroup.LedgerEntryGroupGuid);

            var accountData = result.Groups[0].Contracts.First();
            var account = groupData.Accounts.First();
            Assert.IsNotNull(accountData);

            Assert.AreEqual(account._Id, accountData.Id);
            Assert.AreEqual(account.Description, accountData.ContractName);
            Assert.AreEqual(account.Cost, accountData.Cost);
            Assert.AreEqual(account.InterestDate, accountData.InterestDate);
            Assert.AreEqual(account.ExpectedDate, accountData.ExpectedDate);
            Assert.AreEqual(account.ProcessingDate, accountData.ProcessingDate);
            Assert.AreEqual(account.ContractGuid, accountData.LedgerContractGuid);
        }
    }
}

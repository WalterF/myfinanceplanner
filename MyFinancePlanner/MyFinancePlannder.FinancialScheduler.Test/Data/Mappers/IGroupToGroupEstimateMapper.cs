﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFinancePlanner.FinancialScheduler.Data.Mapper;
using MyFinancePlanner.FinancialScheduler.StartUp;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.FinancialScheduler.Test.Data.Mappers
{
    [TestClass]
    public class IGroupToGroupEstimateMapperTest
    {
        [TestInitialize]
        public void Init()
        {
            ObjectResolver.SetUp();
        }
        [TestMethod, TestCategory("Gateway Mapper")]
        public void GatewayToDataTest()
        {
            var mapper = new IGroupToGroupEstimateMapper();

            IGroup group = new TestEstimateGroup();
            group.GroupName = "Groupname";
            group.GroupType = GroupTypeEnum.Income;
            group.Contracts.Add(new TestEstimateContract() { ContractName = "Contract name", Cost = 5m });

            var result = mapper.GatewayToData(group);
            Assert.AreEqual(result.GroupName, group.GroupName);
            Assert.AreEqual(result.Contracts.First().ContractName, group.Contracts.First().ContractName);

        }
        [TestMethod, TestCategory("Gateway Mapper")]
        public void ContractGatewayToDataTest()
        {
            var mapper = new IGroupToGroupEstimateMapper();

            var contracts = new List<IContract>();
            IContract contract = new TestEstimateContract();
            contract.ContractName = "Walter";
            contracts.Add(contract);

            var results = mapper.GatewayToDatas(contracts);
            Assert.IsNotNull(results);
            var result = results.First();
            Assert.AreEqual(result.ContractName, contract.ContractName);

        }
    }
}

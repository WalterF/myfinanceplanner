﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFinancePlanner.FinancialScheduler.Data;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Data.Gateways;

namespace MyFinancePlanner.FinancialScheduler.Test.Data
{
    [TestClass]
    public class FinancialYearGatewayTest
    {
        [TestMethod]
        public void GetFinancials()
        {
            var gateway = new FinancialYearGateway();
            var result = gateway.GetAll().First();

            Assert.IsNotNull(result);

            var p = (from g in result.Groups
                     from a in g.Accounts
                     where a.ExpectedDate <= DateTime.Now && !a.ProcessingDate.HasValue
                     select a)
                ;

            Assert.IsNotNull(p);
        }

        [TestMethod]
        public void GetPaymentsTest()
        {
            var financialYearId = new Id("aa4c145e-86a9-476e-9497-8b4094fa1e5e");
            var gateway = new FinancialYearGateway();
            var result = gateway.GetPayments(financialYearId);

            Assert.IsNotNull(result);
        }
    }
}

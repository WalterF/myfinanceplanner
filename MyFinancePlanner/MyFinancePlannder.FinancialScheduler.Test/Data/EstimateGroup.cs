﻿using System.Collections.ObjectModel;
using MyFinancePlanner.Core.Base;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.FinancialScheduler.Test.Data
{
    internal class TestEstimateGroup : Model, IGroup
    {
        private readonly Collection<IContract> lst = new Collection<IContract>();

        public string GroupName { get; set; }
        public GroupTypeEnum GroupType { get; set; }

        
        public Collection<IContract> Contracts
        {
            get { return lst; }
        }
    }
}

﻿using MyFinancePlanner.Core.Base;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;

namespace MyFinancePlanner.FinancialScheduler.Test.Data
{
    internal class TestEstimateContract : Model, IContract
    {
        public string ContractName { get; set; }
        public System.DateTime ExpectedProcessingDate { get; set; }
        public System.DateTime StartOn { get; set; }
        public System.DateTime? ExpiredOn { get; set; }
        public MyFinancePlanner.LedgerEntries.Domain.Models.RepeatEnum Period { get; set; }
        public decimal Cost { get; set; }
        public MyFinancePlanner.Core.ObjectValues.Id GroupId { get; set; }
    }
}

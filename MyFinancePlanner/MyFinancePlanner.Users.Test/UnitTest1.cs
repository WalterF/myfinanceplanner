﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.Users.Data.Gateway;
using MyFinancePlanner.Users.Domain;

namespace MyFinancePlanner.Users.Test
{
    [TestClass]
    public class UserTest
    {
        [TestMethod, Ignore()]
        public void CreateUser()
        {
            var user = new User()
            {
                UserName = "Walter",
                Password = "walter12",
                Emailadres = "w.franssen@gmail.com"
            };
            var gateway = new UserGateway();
            var actualUser = gateway.Save(user);

            Assert.IsNotNull(actualUser);
        }

        [TestMethod]
        public void GetUsers()
        {
            var gateway = new UserGateway();
            var users = gateway.GetAll();

            Assert.IsNotNull(users);
        }

        [TestMethod]
        public void GetUserById()
        {
            var userId = new Id(new Guid("749afbcc-ea58-4066-99a1-33ad9f1ee61b"));

            var gateway = new UserGateway();
            var user = gateway.GetById(userId);

            Assert.IsNotNull(user);
        }
    }
}

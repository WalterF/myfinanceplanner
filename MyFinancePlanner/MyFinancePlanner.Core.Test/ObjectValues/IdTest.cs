﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFinancePlanner.Core.ObjectValues;

namespace MyFinancePlanner.Core.Test.ObjectValues
{
    [TestClass]
    public class IdTest
    {
        [TestMethod, TestCategory("Core")]
        public void TestImplicit()
        {
            var guid = Guid.NewGuid();
            var id = new Id(guid);

            Assert.AreEqual<Guid>(guid, id);
        }

        [TestMethod, TestCategory("Core")]
        public void AreEqual()
        {
            var id1 = new Id(new Guid("f455adcf-c3ac-402a-955a-456d106d03a3"));
            var id2 = new Id(new Guid("f455adcf-c3ac-402a-955a-456d106d03a3"));

            Assert.AreEqual(id1, id2);
        }

        [TestMethod, TestCategory("Core")]
        public void AreEqualString()
        {
            var id1 = new Id(new Guid("f455adcf-c3ac-402a-955a-456d106d03a3"));
            var id2 = new Id("f455adcf-c3ac-402a-955a-456d106d03a3");

            Assert.AreEqual(id1, id2);
        }
    }
}

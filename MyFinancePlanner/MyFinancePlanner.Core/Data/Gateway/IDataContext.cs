﻿using MongoDB.Driver;

namespace MyFinancePlanner.Core.Data.Gateway
{
    public interface IDataContext<T> where T : class
    {
        MongoCollection<T> GetCollection(string collectionName);
    }
}

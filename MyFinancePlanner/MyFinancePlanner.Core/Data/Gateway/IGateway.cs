﻿using System.Collections.Generic;
using MyFinancePlanner.Core.ObjectValues;

namespace MyFinancePlanner.Core.Data.Gateway
{
    public interface IGateway<T> where T : class
    {
        T Save(T obj);
        void Delete(Id id);
        List<T> GetAll();
        T GetById(Id id);
    }
}

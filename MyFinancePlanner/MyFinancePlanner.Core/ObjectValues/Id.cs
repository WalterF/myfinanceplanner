﻿using MyFinancePlanner.Core.Base;
using System;

namespace MyFinancePlanner.Core.ObjectValues
{
    public class Id : ValueObject<Id>
    {
        private readonly object _id;
        
        public Id()
        {
            _id = Guid.NewGuid();
        }

        public Id(object id)
        {
            _id = id;
        }
       
        public override bool Equals(object obj)
        {
            var inputId = obj as Id;
            if (inputId == null)
                return false;

            if (inputId.Value is string)
                inputId = new Id(new Guid((string) inputId.Value));

            return this.Value.Equals(inputId.Value);
        }
        public override string ToString()
        {
            return _id.ToString();
        }

        public Object Value { get { return _id; }}

        public static bool TryParse(string s, out Id result)
        {
            result = new Id(s);
            return true;
        }
        public static bool TryParse(Guid s, out Id result)
        {
            result = new Id(s);
            return true;
        }

        public static implicit operator Guid(Id id)
        {
            if (id == null)
                return Guid.Empty;

            return new Guid(id.Value.ToString());
        }


        //public static implicit operator Id(Guid guid)
        //{
        //    return new Id(guid);
        //}

        //public static explicit operator Id(Guid guid)
        //{
        //    return new Id(guid);
        //}
        //public static explicit operator Guid(Id id)
        //{
        //    return id.Value;
        //}
    }
}

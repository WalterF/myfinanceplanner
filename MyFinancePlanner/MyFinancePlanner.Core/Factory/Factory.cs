﻿using StructureMap;
using System.Diagnostics;

namespace MyFinancePlanner.Core.Factory
{
    public class Factory
    {
        private static IContainer Container = new Container();

        [DebuggerStepThrough]
        public static T CreateObject<T>()
        {
            return Container.GetInstance<T>();
        }

        public static void Register<TInterface, TImplementation>() where TImplementation : TInterface
        {
            Container.Configure(c =>
                {
                    c.For<TInterface>().Use<TImplementation>();
                });
        }

    }
}
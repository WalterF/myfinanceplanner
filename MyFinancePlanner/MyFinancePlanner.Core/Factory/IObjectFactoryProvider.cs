using System.Collections.Generic;
using System.Linq;

namespace MyFinancePlanner.Core.Factory
{
    public interface IObjectFactoryProvider
    {
        void Register<TInterface, TImplementation>() where TImplementation : TInterface;
        T CreateObject<T>();
    }

    public class ObjectFactoryProvider
    {
        public static readonly List<IObjectFactoryProvider> Providers = new List<IObjectFactoryProvider>();

        public static void Add(IObjectFactoryProvider provider)
        {
            Providers.Add(provider);
        }

        public static TServiceType CreateObject<TServiceType>() where TServiceType : class
        {
            return Providers.Select(p => p.CreateObject<TServiceType>()).FirstOrDefault(result => result != null);
        }
    }
}
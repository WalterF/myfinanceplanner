﻿using MyFinancePlanner.Core.ObjectValues;

namespace MyFinancePlanner.Core.Base
{
    public interface IModel
    {
        Id Id { get; set; }
    }

    public abstract class Model : IModel
    {
        public Id Id { get; set; }
    }
}

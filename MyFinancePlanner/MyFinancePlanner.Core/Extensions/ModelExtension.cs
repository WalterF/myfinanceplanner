﻿using MyFinancePlanner.Core.Base;
using System;

namespace MyFinancePlanner.Core.Extensions
{
    public static class ModelExtension
    {
        public static void ThrowIfNull(this Model model)
        {
            if (model == null)
                throw new NullReferenceException();
        }
    }
}

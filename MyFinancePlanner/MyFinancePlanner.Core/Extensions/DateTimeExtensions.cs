﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace MyFinancePlanner.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static int GetWeekNo(this DateTime date, CalendarWeekRule calendarWeekRule = CalendarWeekRule.FirstFourDayWeek)
        {
            System.Globalization.Calendar cal = System.Globalization.DateTimeFormatInfo.CurrentInfo.Calendar;
            return cal.GetWeekOfYear(date, calendarWeekRule, DayOfWeek.Monday);
        }
    }
}

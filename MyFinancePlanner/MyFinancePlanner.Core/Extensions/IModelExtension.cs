﻿using MyFinancePlanner.Core.Base;
using System;

namespace MyFinancePlanner.Core.Extensions
{
    public static class IModelExtension
    {
        public static void ThrowIfNull(this IModel model)
        {
            if (model == null)
                throw new NullReferenceException();
        }
    }
}

﻿using System;
using MyFinancePlanner.Core.Data;
using MyFinancePlanner.MongoDb.Data;

namespace MyFinancePlanner.FinancialScheduler.Data
{
    internal class AccountsData : DocumentBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime ExpectedDate { get; set; }
        public DateTime? ProcessingDate { get; set; }
        public DateTime? InterestDate { get; set; }
        public Decimal Cost { get; set; }

        public Guid ContractGuid { get; set; }
    }
}

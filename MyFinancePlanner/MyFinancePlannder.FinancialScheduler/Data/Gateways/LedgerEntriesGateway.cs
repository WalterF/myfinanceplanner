﻿using System.Collections.Generic;
using MyFinancePlanner.FinancialScheduler.Data.Mapper;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;

namespace MyFinancePlanner.FinancialScheduler.Data.Gateways
{
    internal class LedgerEntriesGateway
    {
        private readonly ILedgerEntriesService service;
        private readonly IGroupToGroupEstimateMapper mapper;

        public LedgerEntriesGateway(ILedgerEntriesService service, 
            IGroupToGroupEstimateMapper mapper)
        {
            this.service = service;
            this.mapper = mapper;
        }

        public IEnumerable<EstimateGroupData> GetAlllGroups()
        {
            return mapper.GatewayToData(service.GetAllGroups());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Domain;
using MyFinancePlanner.MongoDb.Data;

namespace MyFinancePlanner.FinancialScheduler.Data.Gateways
{
    class FinancialYearGateway : Gateway<FinancialYearData, MongoDataContext<FinancialYearData>>
    {
        public override string CollectionName { get { return "FinancialYears"; } }
        
        public FinancialYearData GetPayments(Id financialYearId)
        {
            var context = new MongoDataContext<FinancialYearData>();
            var collection = context.GetCollection(CollectionName);

            var builder = Builders<FinancialYear>.Filter;
            var filter = Query.LT("Groups.Accounts.ExpectedDate", BsonDateTime.Create(DateTime.Now));

            var result = collection.FindOneAs<FinancialYearData>(filter);
            return result;
            /*
            var match = new BsonDocument()
            {
                {
                    "Groups.Accounts.ExpectedDate",
                    new BsonDocument
                    {
                        {"$gte", BsonDateTime.Create(DateTime.Now)}

                    }
                }
            };

            var group = new BsonDocument{
                {
                    "$limit", 1
                }
            };

            var pipeline = new[] { match, group };
            AggregateArgs args = new AggregateArgs {Pipeline = pipeline};
            var result = collection.Aggregate(args); //.Match(filter).Group<FinancialYear>;

            var i = result.ToJson();

            return null;
             * */
        }

        
    }
}

﻿using System;
using System.Collections.ObjectModel;
using MyFinancePlanner.Core.Data;
using MyFinancePlanner.MongoDb.Data;

namespace MyFinancePlanner.FinancialScheduler.Data
{
    internal class FinancialYearData : DocumentBase
    {
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal EstimateStartBalance { get; set; }
        public decimal Balance { get; set; }
        public Collection<GroupData> Groups { get; set; }
    }
}

﻿using System.Collections.ObjectModel;
using MyFinancePlanner.Core.Base;

namespace MyFinancePlanner.FinancialScheduler.Data
{
    internal class EstimateGroupData : Model
    {
        private readonly Collection<EstimateContractData> _lst = new Collection<EstimateContractData>();

        public string GroupName { get; set; }
        public MyFinancePlanner.LedgerEntries.Domain.Models.GroupTypeEnum GroupType { get; set; }

        public Collection<EstimateContractData> Contracts
        {
            get { return _lst; }
        }
    }
}

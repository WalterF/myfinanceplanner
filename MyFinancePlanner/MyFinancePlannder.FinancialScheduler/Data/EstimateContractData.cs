﻿using MyFinancePlanner.Core.Base;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.FinancialScheduler.Data
{
    internal class EstimateContractData : Model
    {
        public string ContractName { get; set; }
        public System.DateTime ExpectedProcessingDate { get; set; }
        public System.DateTime StartOn { get; set; }
        public System.DateTime? ExpiredOn { get; set; }
        public RepeatEnum Period { get; set; }
        public decimal Cost { get; set; }
        public Id GroupId { get; set; }
    }
}

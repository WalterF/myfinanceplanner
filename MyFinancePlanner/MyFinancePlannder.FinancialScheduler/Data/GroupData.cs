﻿using System;
using System.Collections.ObjectModel;
using MyFinancePlanner.MongoDb.Data;

namespace MyFinancePlanner.FinancialScheduler.Data
{
    internal class GroupData : DocumentBase
    {
        public string GroupName { get; set; }
        public int GroupType { get; set; }
        public Collection<AccountsData> Accounts { get; set; }
        public Guid GroupGuid { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Domain;
using MyFinancePlanner.FinancialScheduler.Domain.Repositories;
using MyFinancePlanner.FinancialScheduler.Process;

namespace MyFinancePlanner.FinancialScheduler.Service
{
    public class FinancialSchedulerService : IFinancialSchedulerService
    {
        public FinancialYear Create(string title, decimal estimateBalance, DateTime startDate, DateTime endDate)
        {
            return Factory.CreateObject<CreateFinancialYear>().Create(title, estimateBalance, startDate, endDate);
        }

        public FinancialYear Get(Id financialYearId)
        {
            return Factory.CreateObject<GetFinancialYear>().Get(financialYearId);
        }

        public FinancialYear GetCurrent()
        {
            return Factory.CreateObject<GetCurrentFinancialYear>().Do();
        }

        public FinancialYear GetPayments(Id financialYearId)
        {
            return Factory.CreateObject<GetPayments>().Do(financialYearId);
        }

        public void ConfirmPayment(Id financialYearId, Id groupId, Id contractId, decimal cost, DateTime processingDate, DateTime? interestDate)
        {
            Factory.CreateObject<ConfirmPayment>().Do(financialYearId, groupId, contractId, cost, processingDate, interestDate);
        }
        public void AddPayment(Id financialYearId, Id groupId, string contractName, decimal cost, DateTime processingDate, DateTime? interestDate, Id contractGuid)
        {
            Factory.CreateObject<AddPayment>().Do(financialYearId, groupId, contractName, cost, processingDate, interestDate, contractGuid);
        }

        public void RemovePayment(Id financialYearId, Id contractId)
        {
            Factory.CreateObject<RemovePayment>().Do(financialYearId, contractId);
        }

        public IEnumerable<Group> GetGroups(Id financialYearId)
        {
            return Factory.CreateObject<GetGroups>().Do(financialYearId);
        }


        public void Save(FinancialYear financialYear)
        {
            var repository = Factory.CreateObject<FinancialYearRepository>();
            repository.Save(financialYear); 
        }
    }
}

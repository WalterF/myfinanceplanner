﻿using System;
using System.Collections.Generic;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Domain;

namespace MyFinancePlanner.FinancialScheduler.Service
{
    public interface IFinancialSchedulerService
    {
        FinancialYear Create(string title, decimal estimateBalance, DateTime startDate, DateTime endDate);
        FinancialYear Get(Id financialYearId);
        FinancialYear GetCurrent();
        void Save(FinancialYear financialYear);

        FinancialYear GetPayments(Id financialYearId);
        void ConfirmPayment(Id financialYearId, Id groupId, Id contractId, decimal cost, DateTime processingDate, DateTime? interestDate);
        void AddPayment(Id financialYearId, Id groupId, string contractName, decimal cost, DateTime processingDate, DateTime? interestDate, Id contractGuid);
        void RemovePayment(Id financialYearId, Id contractId);

        IEnumerable<Group> GetGroups(Id financialYearId);
        
    }
}
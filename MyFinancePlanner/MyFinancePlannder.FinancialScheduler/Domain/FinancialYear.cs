﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using MyFinancePlanner.Core.Base;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Domain.Factory;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.FinancialScheduler.Domain
{
    public class FinancialYear : Model
    {
        public FinancialYear()
        {
            Groups = new Collection<Group>();
        }

        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal EstimateStartBalance { get; set; }
        public decimal Balance { get; set; }
        public Collection<Group> Groups { get; set; }

        internal Group CreateGroup(string groupname, GroupTypeEnum groupType, Id orginalGroupId)
        {
            var group = GroupFactory.Create(groupname, groupType, orginalGroupId);
            this.Groups.Add(group);

            return group;
        }

        internal bool Pay(Id contractId, decimal cost, DateTime processingDate, DateTime? interestDate)
        {
            var contract = (from g in this.Groups
                from c in g.Contracts
                where c.Id.Equals(contractId)
                select c).FirstOrDefault();

            if (contract != null)
            {
                contract.Cost = cost;
                contract.ProcessingDate = processingDate;
                contract.InterestDate = interestDate;
                return true;
            }
            return false;
        }

        public decimal GetCurrentBalance()
        {
            var currentBalance = this.Balance > 0 ? this.Balance : this.EstimateStartBalance;

            decimal income = (from g in this.Groups
                from a in g.Contracts
                where g.GroupType == GroupTypeEnum.Income && a.ProcessingDate.HasValue
                select a.Cost).Sum();

            decimal cost= (from g in this.Groups
                              from a in g.Contracts
                              where g.GroupType == GroupTypeEnum.Cost && a.ProcessingDate.HasValue
                              select a.Cost).Sum();

            return currentBalance + income - cost;
        }

        public decimal GetEstimatedBalance()
        {
            var currentBalance = this.Balance > 0 ? this.Balance : this.EstimateStartBalance;

            decimal income = (from g in this.Groups
                              from a in g.Contracts
                              where g.GroupType == GroupTypeEnum.Income 
                              select a.Cost).Sum();

            decimal cost = (from g in this.Groups
                            from a in g.Contracts
                            where g.GroupType == GroupTypeEnum.Cost
                            select a.Cost).Sum();

            return currentBalance + income - cost;
        }

        public bool AddPayment(Id groupId, Contract contract)
        {
            var group = this.Groups.FirstOrDefault(x => x.Id.Equals(groupId));
            if (group == null)
                return false;

            group.AddContract(contract);
            return true;
        }

        public void RemovePayment(Id contractId)
        {
            var dto = (from g in this.Groups
                         from c in g.Contracts 
                         where c.Id.Equals(contractId)
                            select new {Group = g, Contract = c}
                        ).FirstOrDefault();

            if (dto != null) dto.Group.Contracts.Remove(dto.Contract);
        }
    }
}

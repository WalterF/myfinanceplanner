﻿using System.Collections.ObjectModel;
using MyFinancePlanner.Core.Base;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.FinancialScheduler.Domain
{
    internal class EstimateGroup : Model
    {
        private readonly Collection<EstimateContract> contracts = new Collection<EstimateContract>();

        public string GroupName { get; set; }
        public GroupTypeEnum GroupType { get; set; }

        public Collection<EstimateContract> Contracts
        {
            get { return contracts; }
        }
    }
}

﻿using System.Collections.Generic;
using MyFinancePlanner.FinancialScheduler.Data;

namespace MyFinancePlanner.FinancialScheduler.Domain.Mapper
{
    internal class EstimateGroupToDataMapper
    {
        public IEnumerable<EstimateGroup> MapToModel(IEnumerable<EstimateGroupData> data)
        {
            return AutoMapper.Mapper.Map<IEnumerable<EstimateGroupData>, IEnumerable<EstimateGroup>>(data);
        }
    }
}

﻿using MyFinancePlanner.FinancialScheduler.Data;

namespace MyFinancePlanner.FinancialScheduler.Domain.Mapper
{
    class FinancialYearMapper
    {
        public FinancialYearData MapToData(FinancialYear model)
        {
            return AutoMapper.Mapper.Map<FinancialYear, FinancialYearData>(model);
        }

        public FinancialYear MapToModel(FinancialYearData data)
        {
            return AutoMapper.Mapper.Map<FinancialYearData, FinancialYear>(data);
        }
    }
}

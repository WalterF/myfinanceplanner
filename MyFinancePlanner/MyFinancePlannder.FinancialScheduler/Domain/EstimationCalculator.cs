﻿using System;
using System.Globalization;
using MyFinancePlanner.FinancialScheduler.Domain.Factory;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.FinancialScheduler.Domain
{
    class EstimationCalculator
    {
        private readonly FinancialYear _financialYear;
        private readonly Group _group;
        private readonly EstimateGroup _estimatedGroup;

        public EstimationCalculator(FinancialYear financialYear, Group group, EstimateGroup estimatedGroup)
        {
            _financialYear = financialYear;
            _group = @group;
            _estimatedGroup = estimatedGroup;
        }

        public void CalculateContracts()
        {
            foreach (var estimateContract in _estimatedGroup.Contracts)
            {
                CalculateBudget(estimateContract);
            }
        }

        private void CalculateBudget(EstimateContract estimateContract)
        {
            if (!IsValidEstimate(estimateContract)) return;
            DateTime startCalculateDate = CalculateStartDate(estimateContract);
            DateTime endCalculateDate = CalculateEndDate(estimateContract);

            if (startCalculateDate >= endCalculateDate) return;

            do
            {
                var contract = ContractFactory.Create(estimateContract.ContractName, estimateContract.Cost,
                    startCalculateDate);
                _group.AddContract(contract);

                //Estimate next cost date
                startCalculateDate = EstimateNextBudgetDate(startCalculateDate, estimateContract);

            } while (startCalculateDate <= endCalculateDate);
        }

        private bool IsValidEstimate(EstimateContract estimateContract)
        {
            return estimateContract.Cost > 0;
        }

        private DateTime EstimateNextBudgetDate(DateTime currentDate, EstimateContract estimateContract)
        {
            DateTime nextEstimateDate = EndOfTheYearDate().AddDays(1);
            switch (estimateContract.Period)
            {
                case RepeatEnum.Month:
                    nextEstimateDate = currentDate.AddMonths(1);
                    break;
                case RepeatEnum.Week:
                    nextEstimateDate = currentDate.AddDays(7);
                    break;
                case RepeatEnum.Year:
                    nextEstimateDate = currentDate.AddYears(1);
                    break;
            }
            return nextEstimateDate;
        }

        private DateTime CalculateEndDate(EstimateContract estimateContract)
        {
            var endOfYear = EndOfTheYearDate();
            if (estimateContract.ExpiredOn.HasValue && estimateContract.ExpiredOn.Value < endOfYear)
            {
                return estimateContract.ExpiredOn.Value;
            }
            return endOfYear;
        }

        internal DateTime CalculateStartDate(EstimateContract estimateContract)
        {
            var startDate = estimateContract.StartOn;
            DateTime nextEstimateDate = EndOfTheYearDate();

            switch (estimateContract.Period)
            {
                case RepeatEnum.Month:
                    nextEstimateDate = new DateTime(_financialYear.StartDate.Year, 1, estimateContract.ExpectedProcessingDate.Day);
                    break;
                case RepeatEnum.Week:
                    nextEstimateDate = FirstDateOfWeek(_financialYear.StartDate.Year, 1, CalendarWeekRule.FirstFullWeek);
                    break;
                case RepeatEnum.Year:
                    nextEstimateDate = new DateTime(_financialYear.StartDate.Year, startDate.Month, startDate.Day);
                    break; 
                case RepeatEnum.None:
                    nextEstimateDate = new DateTime(_financialYear.StartDate.Year, estimateContract.ExpectedProcessingDate.Month,
                        estimateContract.ExpectedProcessingDate.Day);
                    break;
                default:
                    throw new NotImplementedException(String.Format("RepeatEnum : {0} not impelemented", estimateContract.Period.ToString()));
            }
            return nextEstimateDate;
        }

        private DateTime FirstDateOfWeek(int year, int weekNum, CalendarWeekRule rule)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Monday - jan1.DayOfWeek;
            DateTime firstMonday = jan1.AddDays(daysOffset);
           
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstMonday, rule, DayOfWeek.Monday);

            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }

            DateTime result = firstMonday.AddDays(weekNum * 7);
            return result;
        }

        private DateTime EndOfTheYearDate()
        {
            return _financialYear.EndDate;
        }
    }
}

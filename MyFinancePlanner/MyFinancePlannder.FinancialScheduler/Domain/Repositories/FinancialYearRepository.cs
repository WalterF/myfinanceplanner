﻿using System.Linq;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Data.Gateways;
using MyFinancePlanner.FinancialScheduler.Domain.Mapper;

namespace MyFinancePlanner.FinancialScheduler.Domain.Repositories
{
    internal class FinancialYearRepository
    {
        private readonly FinancialYearGateway _gateway;
        private readonly FinancialYearMapper _mapper;

        public FinancialYearRepository(FinancialYearGateway gateway, FinancialYearMapper mapper)
        {
            _gateway = gateway;
            _mapper = mapper;
        }

        public void CreateFinancialYear(FinancialYear financialYear)
        {
            var data = _mapper.MapToData(financialYear);
            _gateway.Save(data);
        }

        public FinancialYear Get(Id financialYear)
        {
            var data = _gateway.GetById(financialYear);
            return _mapper.MapToModel(data);
        }

        public void Save(FinancialYear financialYear)
        {
            var data = _mapper.MapToData(financialYear);
            _gateway.Save(data);
        }

        internal FinancialYear GetPayments(Id financialYearId)
        {
            return _mapper.MapToModel(_gateway.GetPayments(financialYearId));
        }

        public FinancialYear GetCurrent()
        {
            var data = _gateway.GetAll().FirstOrDefault();
            return _mapper.MapToModel(data);
        }
    }
}

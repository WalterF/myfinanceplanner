﻿using System;
using MyFinancePlanner.Core.Base;
using MyFinancePlanner.Core.ObjectValues;

namespace MyFinancePlanner.FinancialScheduler.Domain
{
    public class Contract : Model
    {
        public string ContractName { get; set; }
        public decimal Cost { get; set; }
        public DateTime ExpectedDate { get; set; }
        public DateTime? InterestDate { get; set; }
        public DateTime? ProcessingDate { get; set; }
        public Id LedgerContractGuid { get; set; }
    }
}

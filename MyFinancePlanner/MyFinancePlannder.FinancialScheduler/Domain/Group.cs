﻿using System.Collections.ObjectModel;
using MyFinancePlanner.Core.Base;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.FinancialScheduler.Domain
{
    public class Group : Model
    {
        public string GroupName { get; set; }
        public GroupTypeEnum GroupType { get; set; }
        public Id LedgerEntryGroupGuid { get; set; }
        public Collection<Contract> Contracts { get; set; }

        public void AddContract(Contract contract)
        {
            if (Contracts == null) Contracts = new Collection<Contract>();

            Contracts.Add(contract);
        }
    }
}

﻿using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.FinancialScheduler.Domain.Factory
{
    internal static class GroupFactory
    {
        public static Group Create(string groupname, GroupTypeEnum groupType, Id orginalGroupId)
        {
            var group = new Group();
            group.Id = new Id();
            group.GroupName = groupname;
            group.GroupType = groupType;
            group.LedgerEntryGroupGuid = orginalGroupId;

            return group;
        }
    }
}

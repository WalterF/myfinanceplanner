﻿using System;
using MyFinancePlanner.Core.ObjectValues;

namespace MyFinancePlanner.FinancialScheduler.Domain.Factory
{
    static class ContractFactory
    {
        public static Contract Create(string contractName, decimal cost, DateTime expectedDate)
        {
            return new Contract()
            {
                Id = new Id(),
                ContractName = contractName,
                Cost = cost,
                ExpectedDate = expectedDate
            };
        }
        public static Contract Create(string contractName, decimal cost, DateTime processingDate, DateTime? interestDate, Id contractGuid)
        {
            return new Contract()
            {
                Id = new Id(),
                ContractName = contractName,
                Cost = cost,
                ExpectedDate = processingDate,
                ProcessingDate = processingDate,
                InterestDate = interestDate,
                LedgerContractGuid = contractGuid
            };
        }
    }
}

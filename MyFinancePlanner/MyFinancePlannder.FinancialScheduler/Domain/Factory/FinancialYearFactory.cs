﻿using System;
using MyFinancePlanner.Core.ObjectValues;

namespace MyFinancePlanner.FinancialScheduler.Domain.Factory
{
    internal static class FinancialYearFactory
    {
        public static FinancialYear Create(string title, decimal estimateBalance, DateTime startDate, DateTime endDate)
        {
            FinancialYear financialYear = new FinancialYear()
            {
                Id = new Id(), 
                Title = title,
                EstimateStartBalance = estimateBalance,
                StartDate = startDate,
                EndDate = endDate
            };
            return financialYear;
        }
    }
}
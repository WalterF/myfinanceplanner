﻿using System;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Domain.Repositories;

namespace MyFinancePlanner.FinancialScheduler.Process
{
    class ConfirmPayment
    {
        public void Do(Id financialYearId, Id groupId, Id contractId, decimal cost, 
            DateTime processingDate, DateTime? interestDate)
        {
            var financialYear = Factory.CreateObject<GetPayments>().Do(financialYearId);
            if (financialYear.Pay(contractId, cost, processingDate, interestDate))
            {
                var repository = Factory.CreateObject<FinancialYearRepository>();
                repository.Save(financialYear);    
            }
        }
    }
}

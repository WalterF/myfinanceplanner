﻿using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Domain.Repositories;

namespace MyFinancePlanner.FinancialScheduler.Process
{
    class RemovePayment
    {
        public void Do(Id financialYearId, Id contractId)
        {
            var financialYearRepository = Factory.CreateObject<FinancialYearRepository>();
            var financialYear = financialYearRepository.Get(financialYearId);

            financialYear.RemovePayment(contractId);

            financialYearRepository.Save(financialYear);
        }
    }
}

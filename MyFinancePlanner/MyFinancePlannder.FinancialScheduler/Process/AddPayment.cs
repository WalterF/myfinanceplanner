﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Domain.Factory;
using MyFinancePlanner.FinancialScheduler.Domain.Repositories;

namespace MyFinancePlanner.FinancialScheduler.Process
{
    class AddPayment
    {
        public bool Do(Id financialYearId, Id groupId, string contractName, decimal cost, DateTime processingDate, DateTime? interestDate, Id contractGuid)
        {
            var financialYear = Factory.CreateObject<GetPayments>().Do(financialYearId);
            if (financialYear == null)
                return false;

            var contract = ContractFactory.Create(contractName, cost, processingDate, interestDate, contractGuid);

            if (financialYear.AddPayment(groupId, contract))
            {
                var repository = Factory.CreateObject<FinancialYearRepository>();
                repository.Save(financialYear);
                return true;
            }

            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.FinancialScheduler.Domain;
using MyFinancePlanner.FinancialScheduler.Domain.Factory;
using MyFinancePlanner.FinancialScheduler.Domain.Repositories;

namespace MyFinancePlanner.FinancialScheduler.Process
{
    class CreateFinancialYear
    {
        internal FinancialYear Create(string title, decimal estimateBalance, DateTime startDate, DateTime endDate)
        {
            FinancialYear financialYear = FinancialYearFactory.Create(title, estimateBalance, startDate, endDate);

            // Create groups and estimations
            var groups = Factory.CreateObject<EstimationRepository>().GetEstimateGroups();
            CalculateEstimates(financialYear, groups);

            var financialYearRepository = Factory.CreateObject<FinancialYearRepository>();
            financialYearRepository.CreateFinancialYear(financialYear);

            return financialYear;
        }

        private void CalculateEstimates(FinancialYear financialYear, IEnumerable<EstimateGroup> estimatedGroups)
        {
            if (estimatedGroups == null) return;

            foreach (var estimatedGroup in estimatedGroups)
            {
                var group = financialYear.CreateGroup(estimatedGroup.GroupName, estimatedGroup.GroupType, estimatedGroup.Id);

                var estimationCalculator = new EstimationCalculator(financialYear, group, estimatedGroup);
                estimationCalculator.CalculateContracts();
            }
        }
    }
}

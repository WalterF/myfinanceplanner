﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Domain;
using MyFinancePlanner.FinancialScheduler.Domain.Repositories;

namespace MyFinancePlanner.FinancialScheduler.Process
{
    class GetGroups
    {
        public IEnumerable<Group> Do(Id financialYearId)
        {
            var financialYear = Factory.CreateObject<FinancialYearRepository>()
                .Get(financialYearId);

            return financialYear.Groups;
        }
    }
}

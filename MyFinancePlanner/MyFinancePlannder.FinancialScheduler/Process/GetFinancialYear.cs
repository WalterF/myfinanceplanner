﻿using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Domain;
using MyFinancePlanner.FinancialScheduler.Domain.Repositories;

namespace MyFinancePlanner.FinancialScheduler.Process
{
    class GetFinancialYear
    {
        public FinancialYear Get(Id id)
        {
            return Factory.CreateObject<FinancialYearRepository>().Get(id);
        }
    }
}

﻿using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.FinancialScheduler.Domain;
using MyFinancePlanner.FinancialScheduler.Domain.Repositories;

namespace MyFinancePlanner.FinancialScheduler.Process
{
    class GetCurrentFinancialYear
    {
        public FinancialYear Do()
        {
            return Factory.CreateObject<FinancialYearRepository>().GetCurrent();
        }
    }
}

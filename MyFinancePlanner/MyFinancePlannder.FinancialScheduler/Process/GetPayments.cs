﻿using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Domain;
using MyFinancePlanner.FinancialScheduler.Domain.Repositories;

namespace MyFinancePlanner.FinancialScheduler.Process
{
    class GetPayments
    {
        public FinancialYear Do(Id financialYearId)
        {
            var financialYearRepository = Factory.CreateObject<FinancialYearRepository>();
            return financialYearRepository.GetPayments(financialYearId);
        }
    }
}

﻿using System.Diagnostics;
using AutoMapper;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.FinancialScheduler.Data;
using MyFinancePlanner.FinancialScheduler.Domain;

namespace MyFinancePlanner.FinancialScheduler.StartUp
{
    public class FinancialYearMapperProfile : Profile
    {

        protected override void Configure()
        {
            Mapper.CreateMap<FinancialYear, FinancialYearData>()
                .ForMember(d => d._Id, o => o.MapFrom(s => s.Id))
                .AfterMap((d, m) => Debug.WriteLine("year data to model"))
                .ReverseMap()
                .ForMember(d => d.Id, o => o.MapFrom(s => new Id(s._Id)))
                .AfterMap((d, m) => Debug.WriteLine("year model to data"))
                ;
            
            Mapper.CreateMap<Group, GroupData>()
                .ForMember(d => d._Id, o => o.MapFrom(s => s.Id))
                .ForMember(d => d.Accounts, o => o.MapFrom(s => s.Contracts))
                .ForMember(d => d.GroupGuid, o => o.MapFrom(s => s.LedgerEntryGroupGuid))
                .AfterMap((d, m) => Debug.WriteLine("group data to model"))
                .ReverseMap()
                .ForMember(d => d.Id, o => o.MapFrom(s => new Id(s._Id)))
                .ForMember(d => d.Contracts, o => o.MapFrom(s => s.Accounts))
                .ForMember(d => d.LedgerEntryGroupGuid, o => o.MapFrom(s => new Id(s.GroupGuid)))
                .AfterMap((d, m) => Debug.WriteLine("group model to data"))
                ;
            
            Mapper.CreateMap<Contract, AccountsData>()
                .ForMember(d => d._Id, o => o.MapFrom(s => s.Id))
                .ForMember(d => d.Description, o => o.MapFrom(s => s.ContractName))
                .ForMember(d => d.ContractGuid, o => o.MapFrom(s => s.LedgerContractGuid))
                .AfterMap((d, m) => Debug.WriteLine("accountdata to model"))
                .ReverseMap()
                .ForMember(d => d.Id,  o => o.MapFrom(s => new Id(s._Id)))
                .ForMember(d => d.ContractName, o => o.MapFrom(s => s.Description))
                .ForMember(d => d.LedgerContractGuid, o => o.MapFrom(s => new Id(s.ContractGuid)))
                .AfterMap((d, m) => Debug.WriteLine("Contract model to data"))
                ;
        }
    }
}
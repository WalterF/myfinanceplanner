﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.LedgerEntries.Domain.Factory;
using MyFinancePlanner.LedgerEntries.Domain.Models;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;

namespace MyFinancePlanner.LedgerEntries.Test
{
    [TestClass]
    public class CreateData
    {
        [TestInitialize]
        public void Init()
        {
            StartUp.ObjectResolver.SetUp();
        }
        [TestMethod, TestCategory("Create Test Data"), Ignore()]
        public void Create2016Data()
        {
            var inkomsten = GroupFactory.Create("Inkomsten", GroupTypeEnum.Income);
            inkomsten.AddContract("Salaris", 2525m, new DateTime(2013, 4, 25), new DateTime(2013, 4, 1), null);
            inkomsten.AddContract("reiskosten decl.", 200, new DateTime(2013, 4, 25), new DateTime(2013, 4, 1));
            inkomsten.AddContract("Huur Sint Joseph.", 725, new DateTime(2013, 12, 6), new DateTime(2013, 12, 1));
            inkomsten.AddContract("Overig", 0, new DateTime(2015, 1, 1), new DateTime(2016, 1, 1), null, RepeatEnum.None);
            inkomsten.AddContract("Belasting dienst", 45, new DateTime(2015, 1, 15), new DateTime(2015, 1, 15));
            inkomsten.AddContract("van spaarrekening", 0, new DateTime(2015, 1, 1), new DateTime(2015, 1, 1), null, RepeatEnum.None);

            var camphaag = GroupFactory.Create("Camphaag 36", GroupTypeEnum.Cost);
            camphaag.AddContract("Hypotheek", 554.61m, new DateTime(2011, 10, 28), new DateTime(2011, 10, 28), new DateTime(2041, 10, 28), RepeatEnum.Month);
            camphaag.AddContract("AEGON (levensverzekering)", 14.41m, new DateTime(2011, 10, 4), new DateTime(2011, 10, 4));
            camphaag.AddContract("Wegenbelasting", 99, new DateTime(2015, 6, 1), new DateTime(2015, 6, 1), null, RepeatEnum.Month, 3);
            camphaag.AddContract("WML", 19.6m, new DateTime(2011, 10, 28), new DateTime(2011, 10, 18));
            camphaag.AddContract("Belasting", 600, new DateTime(2011, 10, 28), new DateTime(2011, 10, 28), null, RepeatEnum.None);
            camphaag.AddContract("Oxxio", 117m, new DateTime(2016, 1, 5), new DateTime(2016, 1, 27), new DateTime(2017, 1, 5));
            camphaag.AddContract("Anderzorg (2015)", 216.94m, new DateTime(2016, 1, 11), new DateTime(2016, 1, 1), new DateTime(2016, 12, 31));
            camphaag.AddContract("T-Mobile (Walter)", 35m, new DateTime(2015, 6, 7), new DateTime(2015, 6, 1), new DateTime(2017, 6, 15));
            camphaag.AddContract("T-Mobile (Katja)", 38m, new DateTime(2015, 6, 7), new DateTime(2015, 6, 1), new DateTime(2017, 6, 15));
            camphaag.AddContract("Ziggo", 86m, new DateTime(2012, 1, 5), new DateTime(2012, 1, 1));
            camphaag.AddContract("Netflix", 7.99m, new DateTime(2013, 1, 21), new DateTime(2013, 1, 1));
            camphaag.AddContract("NN (Rvs)", 38.94m, new DateTime(2008, 10, 8), new DateTime(2008, 10, 1), new DateTime(2044, 10, 28));
            camphaag.AddContract("Reaal Schadeverzekering", 52.47m, new DateTime(2012, 1, 5), new DateTime(2012, 1, 1));
            camphaag.AddContract("Voogd en Voogd Verzekeringen", 34.83m, new DateTime(2015, 6, 5), new DateTime(2015, 6, 1));
            camphaag.AddContract("Reality", 25m, new DateTime(2015, 7, 28), new DateTime(2015, 7, 1));
            camphaag.AddContract("Sportdomain", 42m, new DateTime(2013, 1, 28), new DateTime(2013, 1, 1));
            camphaag.AddContract("Top vorm", 25m, new DateTime(2015, 10, 28), new DateTime(2015, 10, 1));
            camphaag.AddContract("Rabobank", 3.2m, new DateTime(2011, 7, 28), new DateTime(2011, 7, 28));
            camphaag.AddContract("Creditcard", 10m, new DateTime(2011, 10, 28), new DateTime(2011, 10, 1));
            camphaag.AddContract("Sparen Sammy", 50m, new DateTime(2013, 1, 28), new DateTime(2013, 1, 15));
            camphaag.AddContract("Sparen", 200m, new DateTime(2011, 10, 1), new DateTime(2011, 10, 28), period:RepeatEnum.None);
            camphaag.AddContract("Dela", 9.64m, new DateTime(2003, 1, 28), new DateTime(2003, 1, 1));
            camphaag.AddContract("Uwhuisbeveiliging", 37.45m, new DateTime(2012, 6, 5), new DateTime(2012, 6, 1));
            camphaag.AddContract("Pensioen", 200m, new DateTime(2014, 12, 8), new DateTime(2014, 12, 8));
            camphaag.AddContract("Tanken", 50m, new DateTime(2013, 10, 1), new DateTime(2013, 10, 1), null, RepeatEnum.Week);
            camphaag.AddContract("Overige", 300m, new DateTime(2013, 10, 1), new DateTime(2013, 10, 1));

            var sintJoseph = GroupFactory.Create("Sint Josephstraat 10c", GroupTypeEnum.Income);
            sintJoseph.AddContract("Hypotheek 10c", 336.52m, new DateTime(2003, 1, 1), new DateTime(2003, 1, 1));
            sintJoseph.AddContract("Belasting", 0m, new DateTime(2016, 4, 1), new DateTime(2016, 4, 1));
            sintJoseph.AddContract("Reaal Levensverzekering", 146.36m, new DateTime(2003, 1, 1), new DateTime(2003, 1, 1));
            sintJoseph.AddContract("Wml", 10.8m, new DateTime(2016, 1, 15), new DateTime(2016, 1, 15));
            sintJoseph.AddContract("Oxxio", 91m, new DateTime(2016, 1, 24), new DateTime(2016, 1, 24), new DateTime(2017, 1, 24));
            sintJoseph.AddContract("Volta", 28.5m, new DateTime(2016, 1, 28), new DateTime(2016, 1, 28));
            sintJoseph.AddContract("VVE", 300m, new DateTime(2003, 1, 28), new DateTime(2003, 1, 28));
            sintJoseph.AddContract("Sparen", 300m, new DateTime(2013, 1, 10), new DateTime(2013, 1, 10));

            var service = Factory.CreateObject<ILedgerEntriesService>();
            var newInkomsten = service.SaveGroup(inkomsten);
            var newCamphaag = service.SaveGroup(camphaag);
            var newSintJoseph = service.SaveGroup(sintJoseph);

            Assert.IsNotNull(newInkomsten);
            Assert.IsNotNull(newCamphaag);
            Assert.IsNotNull(newSintJoseph);
        }

        [TestMethod, TestCategory("Create Test Data")]
        public void GetGroups()
        {
            var service = Factory.CreateObject<ILedgerEntriesService>();
            var groups = service.GetAllGroups();

            Assert.IsNotNull(groups);
            Assert.IsNotNull(groups.First().Contracts);

            /*
            var groupToDelete = groups.FirstOrDefault(x => x.Id == new Guid("fb8f19d9-2b23-400e-9958-82761d841ad4"));
            Assert.IsNotNull(groupToDelete);
            var  result = service.DeleteGroup(groupToDelete);
            Assert.IsTrue(result);
            */
        }

        /* test omgeving */
        [TestMethod, TestCategory("Create Test Data")]
        public void CreateTest2016Data()
        {
            var inkomsten = GroupFactory.Create("Inkomsten", GroupTypeEnum.Income);
            inkomsten.AddContract("Salaris", 1500m, new DateTime(2013, 4, 25), new DateTime(2013, 4, 1), null);
            inkomsten.AddContract("Belasting dienst", 50, new DateTime(2015, 1, 15), new DateTime(2015, 1, 15));
            inkomsten.AddContract("van spaarrekening", 0, new DateTime(2015, 1, 1), new DateTime(2015, 1, 1), null, RepeatEnum.None);

            var camphaag = GroupFactory.Create("Camphaag 36", GroupTypeEnum.Cost);
            camphaag.AddContract("Hypotheek", 300m, new DateTime(2011, 10, 28), new DateTime(2011, 10, 28), new DateTime(2041, 10, 28), RepeatEnum.Month);
            camphaag.AddContract("AEGON (levensverzekering)", 10m, new DateTime(2011, 10, 4), new DateTime(2011, 10, 4));
            camphaag.AddContract("Wegenbelasting", 99, new DateTime(2015, 6, 1), new DateTime(2015, 6, 1), null, RepeatEnum.Month, 3);
            camphaag.AddContract("WML", 100m, new DateTime(2011, 10, 28), new DateTime(2011, 10, 18));
            camphaag.AddContract("Gas en licht", 117m, new DateTime(2016, 1, 5), new DateTime(2016, 1, 27), new DateTime(2017, 1, 5));
            camphaag.AddContract("Zorgverzekering (2016)", 130m, new DateTime(2016, 1, 11), new DateTime(2016, 1, 1), new DateTime(2016, 12, 31));
            camphaag.AddContract("T-Mobile", 35m, new DateTime(2015, 6, 7), new DateTime(2015, 6, 1), new DateTime(2017, 6, 15));
            camphaag.AddContract("Ziggo", 86m, new DateTime(2012, 1, 5), new DateTime(2012, 1, 1));
            camphaag.AddContract("Netflix", 7.99m, new DateTime(2013, 1, 21), new DateTime(2013, 1, 1));
            camphaag.AddContract("Auto Verzekeringen", 30m, new DateTime(2015, 6, 5), new DateTime(2015, 6, 1));
            camphaag.AddContract("Top vorm", 25m, new DateTime(2015, 10, 28), new DateTime(2015, 10, 1));
            camphaag.AddContract("Rabobank", 3.2m, new DateTime(2011, 7, 28), new DateTime(2011, 7, 28));
            camphaag.AddContract("Creditcard", 10m, new DateTime(2011, 10, 28), new DateTime(2011, 10, 1));
            camphaag.AddContract("Sparen", 50m, new DateTime(2011, 10, 1), new DateTime(2011, 10, 28), period: RepeatEnum.None);
            camphaag.AddContract("Overige", 300m, new DateTime(2013, 10, 1), new DateTime(2013, 10, 1));

            
            var service = Factory.CreateObject<ILedgerEntriesService>();
            var newInkomsten = service.SaveGroup(inkomsten);
            var newCamphaag = service.SaveGroup(camphaag);
            
            Assert.IsNotNull(newInkomsten);
            Assert.IsNotNull(newCamphaag);

        }
    }
}

﻿using System;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFinancePlanner.LedgerEntries.Data;
using MyFinancePlanner.LedgerEntries.Domain.Mapper;
using MyFinancePlanner.LedgerEntries.StartUp;
using MyFinancePlanner.LedgerEntries.Domain.Models;
using MyFinancePlanner.Core.ObjectValues;

namespace FinancePlanner.EstimationService.Test.Domain.Mapper
{
    [TestClass]
    public class GroupMapperTest
    {
        [TestInitialize]
        public void Init()
        {
            ObjectResolver.SetUp();
        }

        [TestMethod, TestCategory("Mapper")]
        public void MapDataToModel()
        {
            var data = new GroupData()
                {
                    _Id = Guid.NewGuid(), GroupType=1, GroupName="UnitTestGroup"
                };

            var model = GroupMapper.MapToModel(data);

            Assert.IsNotNull(model);
            Assert.AreEqual<Guid>(data._Id, model.Id);
            Assert.AreEqual<string>(data.GroupName, model.GroupName);
            Assert.AreEqual<int>(data.GroupType, (int)model.GroupType);
        }

        [TestMethod, TestCategory("Mapper")]
        public void MapModelToData()
        {
            var model = new Group()
            {
                Id = new Id(),
                GroupType = GroupTypeEnum.Income,
                GroupName = "UnitTestGroup"
            };

            var data = GroupMapper.MapToData(model);

            Assert.IsNotNull(data);
            Assert.AreEqual<Guid>(model.Id, data._Id);
            Assert.AreEqual<string>(model.GroupName, data.GroupName);
            Assert.AreEqual<int>((int)model.GroupType, data.GroupType);
        }

        [TestMethod, TestCategory("Mapper")]
        public void MapModelToDataAndChilds()
        {
            var model = new Group()
            {
                Id = new Id(),
                GroupType = GroupTypeEnum.Income,
                GroupName = "UnitTestGroup"               
            };
            model.Contracts.Add(new Contract() { Id = new Id(), GroupId = model.Id, ContractName = "ContractName 1" });
            model.Contracts.Add(new Contract() { Id = new Id(), GroupId = model.Id, ContractName = "ContractName 2" });
            model.Contracts.Add(new Contract() { Id = new Id(), GroupId = model.Id, ContractName = "ContractName 3" });

            var data = GroupMapper.MapToData(model);

            Assert.IsNotNull(data);
            Assert.AreEqual(model.Contracts.Count, data.Contracts.Count);
        }
        [TestMethod, TestCategory("Mapper")]
        public void MapDataToModelAndChilds()
        {
            var data = new GroupData()
            {
                _Id = Guid.NewGuid(),
                GroupType = 1,
                GroupName = "UnitTestGroupData",
                Contracts =  new Collection<ContractData>()
            };
            data.Contracts.Add(new ContractData() { _Id = Guid.NewGuid(), GroupGuid = data._Id, ContractName = "ContractName 1" });
            data.Contracts.Add(new ContractData() { _Id = Guid.NewGuid(), GroupGuid = data._Id, ContractName = "ContractName 2" });
            data.Contracts.Add(new ContractData() { _Id = Guid.NewGuid(), GroupGuid = data._Id, ContractName = "ContractName 3" });

            var model = GroupMapper.MapToModel(data);

            Assert.IsNotNull(model);
            Assert.AreEqual(data._Id, model.Id);
            Assert.AreEqual(data.Contracts.Count, model.Contracts.Count);
            Assert.AreEqual(data.Contracts[0]._Id, model.Contracts[0].Id);
            Assert.AreEqual(data.Contracts[1]._Id, model.Contracts[1].Id);
            Assert.AreEqual(data.Contracts[2]._Id, model.Contracts[2].Id);
            Assert.AreEqual(data.Contracts[0].GroupGuid, model.Contracts[0].GroupId);
            Assert.AreEqual(data.Contracts[1].GroupGuid, model.Contracts[1].GroupId);
            Assert.AreEqual(data.Contracts[2].GroupGuid, model.Contracts[2].GroupId);
        }
    }
}

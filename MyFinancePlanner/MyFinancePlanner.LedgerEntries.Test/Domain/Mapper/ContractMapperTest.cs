﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFinancePlanner.LedgerEntries.StartUp;
using MyFinancePlanner.LedgerEntries.Domain.Factory;
using MyFinancePlanner.LedgerEntries.Domain.Models;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.LedgerEntries.Domain.Mapper;
using MyFinancePlanner.LedgerEntries.Data;

namespace MyFinancePlanner.LedgerEntries.Test.Domain.Mapper
{
    [TestClass]
    public class ContractMapperTest
    {
        [TestInitialize]
        public void Init()
        {
            ObjectResolver.SetUp();
        }

        [TestMethod, TestCategory("Mapper")]
        public void MapDataToData()
        {
            var model = ContractFactory.Create("Essent", 38m, DateTime.Now, DateTime.Now, DateTime.Now.AddMonths(12), 
                RepeatEnum.Month, 1, new Id());

            var data = ContractMapper.MapToData(model);

            Assert.IsNotNull(data);
            Assert.AreEqual<Guid>(model.Id, data._Id);
            Assert.AreEqual<Guid>(model.GroupId, data.GroupGuid);
            Assert.AreEqual<string>(model.ContractName, data.ContractName);
            Assert.AreEqual<decimal>(model.Cost, data.Cost);
            Assert.AreEqual<DateTime>(model.ExpectedProcessingDate, data.ExpectedProcessingDate);
            Assert.AreEqual<DateTime>(model.StartOn, data.StartOn);
            Assert.AreEqual<DateTime?>(model.ExpiredOn, data.ExpiredOn);
            Assert.AreEqual<int>((int)model.Period, (int)data.Period);
            
        }

        [TestMethod, TestCategory("Mapper")]
        public void MapDataToModel()
        {
            var data = new ContractData()
            {
                _Id = Guid.NewGuid(), GroupGuid = Guid.NewGuid(), 
                ContractName = "Essent", Cost = 35m, StartOn=DateTime.Now, 
                ExpiredOn=DateTime.Now.AddYears(1), Period=RepeatEnum.Month
            };

            var model = ContractMapper.MapToModel(data);

            Assert.IsNotNull(model);
            Assert.AreEqual<Guid>(data._Id, model.Id);
            Assert.AreEqual<Guid>(data.GroupGuid, model.GroupId);
            Assert.AreEqual<string>(data.ContractName, model.ContractName);
            Assert.AreEqual<decimal>(data.Cost, model.Cost);
            Assert.AreEqual<DateTime>(data.StartOn, model.StartOn);
            Assert.AreEqual<DateTime?>(data.ExpiredOn, model.ExpiredOn);
            Assert.AreEqual<int>((int)data.Period, (int)model.Period);
        }
    }
}

using System.Configuration;
using MongoDB.Driver;
using MyFinancePlanner.Core.Data.Gateway;

namespace MyFinancePlanner.MongoDb.Data
{
    public class MongoDataContext<T> : IDataContext<T> where T : class
    {
        private MongoDatabase GetDatabase()
        {
            var mongoDbName = ConfigurationManager.AppSettings["MongoDbName"];
            var mongoUrl = ConfigurationManager.AppSettings["MongoUrl"];
            //var connectionString = "mongodb://MyFinancePlanner:FranssenKatjaSammyPippaQuinnPippa15#2@85.214.199.71:27017/MyFinancePlanner";
            var connectionString = string.Format("{0}{1}", mongoUrl, mongoDbName);
            //mongodb://85.214.199.71:27017/MyFinancePlanner";
            var mongoClient = new MongoClient(connectionString);

            var database = mongoClient.GetServer().GetDatabase(mongoDbName);
            
            return database;
        }

        private MongoDatabase dataContext = null;
        public MongoDatabase DataContext {
            get {
                if (dataContext == null)
                    dataContext = GetDatabase();

                return dataContext;
            }
        }

        public MongoCollection<T> GetCollection(string collectionName)
        {
            return DataContext.GetCollection<T>(collectionName);
        }
    }
}

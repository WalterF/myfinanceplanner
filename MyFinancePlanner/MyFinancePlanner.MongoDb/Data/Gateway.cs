using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MyFinancePlanner.Core.Data.Gateway;
using MyFinancePlanner.Core.ObjectValues;

namespace MyFinancePlanner.MongoDb.Data
{

    public abstract class Gateway<T, C> : IGateway<T>
        where T : class
        where C : IDataContext<T>, new()
    {
        public abstract string CollectionName { get; }

        public T Save(T entity)
        {
            var context = new C();
            var result =  context.GetCollection(CollectionName).Save<T>(entity);

            return entity;
        }

        public void Delete(Id id)
        {
            var context = new C();
            var query = Query.EQ("_id", (Guid)id);
            context.GetCollection(CollectionName).Remove(query, RemoveFlags.Single, WriteConcern.Acknowledged);
            //return result.DocumentsAffected > 0;
        }

        public List<T> GetAll()
        {
            var context = new C();
            var result = context.GetCollection(CollectionName).FindAllAs<T>();
            return result.ToList<T>();
        }

        public T GetById(Id id)
        {
            var context = new C();
            var query = Query.EQ("_id", (Guid)id);
            var result = context.GetCollection(CollectionName).FindOneAs<T>(query);

            return result;
        }

        public T Argg()
        {
            var context = new C();
            AggregateArgs arg = new AggregateArgs();
            var result = context.GetCollection(CollectionName).Aggregate(arg);

            return null;
        }
    }
}

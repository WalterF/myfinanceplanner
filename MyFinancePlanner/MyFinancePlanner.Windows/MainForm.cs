﻿using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Windows.Domain.Factory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyFinancePlanner.Windows
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void groupsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = FormFactory.CreateMdiForm<GroupOverviewForm>(this);
        }

        private void contractsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormFactory.CreateMdiForm<ContractOverviewForm>(this);
        }
    }
}

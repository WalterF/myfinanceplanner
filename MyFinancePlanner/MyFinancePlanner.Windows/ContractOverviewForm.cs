﻿using MyFinancePlanner.Windows.Utils.Extensions;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using MyFinancePlanner.LedgerEntries.Domain.Models;
using MyFinancePlanner.Windows.Domain.Factory;
using MyFinancePlanner.LedgerEntries.Domain.Factory;

namespace MyFinancePlanner.Windows
{
    public partial class ContractOverviewForm : Form
    {
        private ILedgerEntriesService service = Factory.CreateObject<ILedgerEntriesService>();

        public ContractOverviewForm()
        {
            InitializeComponent();

            this.Load += ContractOverviewForm_Load;
        }

        void ContractOverviewForm_Load(object sender, EventArgs e)
        {
            try
            {
                InitGrid();
                LoadData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LoadData()
        {
            dataGridView1.DataSource = service.GetAllContracts();
        }
        private void InitGrid()
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.AutoSize = true;

            dataGridView1.AddTextBoxColumn("ContractName", "ContractName");
            dataGridView1.AddTextBoxColumn("Cost", "Cost");
            dataGridView1.AddTextBoxColumn("StartOn", "StartOn");
            dataGridView1.AddTextBoxColumn("ExpiredOn", "ExpiredOn");
            dataGridView1.AddTextBoxColumn("ContractName", "ContractName");
            dataGridView1.AddEnumColumn<RepeatEnum>("Period", "Period");

        }

        private void addContractToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var frm = FormFactory.CreateDialogForm<ContractEdit>(this))
            {
                //var newContract = ContractFactory.Create("", 0, DateTime.Now, null, RepeatEnum.Month, null);
                //frm.Init(newContract);
                //frm.ShowDialog();
            }

        }
    }
}

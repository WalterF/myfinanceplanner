﻿using MyFinancePlanner.Core.Base;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.Windows.Domain
{
    public class Group : Model
    {
        public string GroupName { get; set; }
        public GroupTypeEnum GroupType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyFinancePlanner.Core.Factory;
using System.Windows.Forms;

namespace MyFinancePlanner.Windows.Domain.Factory
{
    public static class FormFactory
    {
        public static T CreateMdiForm<T>(Form creator) where T : Form
        {
            var frm = Core.Factory.Factory.CreateObject<T>();
            frm.MdiParent = creator;
            frm.WindowState = FormWindowState.Maximized;
            frm.Show();

            return frm;
        }

        public static T CreateDialogForm<T>(Form creator) where T : Form
        {
            var frm = Core.Factory.Factory.CreateObject<T>();
            frm.Owner = creator;

            return frm;
        }
    }
}

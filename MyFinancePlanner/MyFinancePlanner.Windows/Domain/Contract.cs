﻿using MyFinancePlanner.Core.Base;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.LedgerEntries.Domain.Models;
using System;

namespace MyFinancePlanner.Windows.Domain
{
    public class Contract : Model
    {
        public string ContractName { get; set; }
        public DateTime StartOn { get; set; }
        public DateTime? ExpiredOn { get; set; }
        public RepeatEnum Period { get; set; }
        public decimal Cost { get; set; }
        public Id GroupId { get; set; }
    }
}

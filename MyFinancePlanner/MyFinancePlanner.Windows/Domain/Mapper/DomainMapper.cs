﻿namespace MyFinancePlanner.Windows.Domain.Mapper
{
    public class DomainMapper<TAppDomain, TComponentDomain>
    {
        public TAppDomain MapToAppDomain(TComponentDomain componentDomain)
        {
            return AutoMapper.Mapper.Map<TComponentDomain, TAppDomain>(componentDomain);
        }

        public TComponentDomain MapToComponentDomain(TAppDomain appDomain)
        {
            return AutoMapper.Mapper.Map<TAppDomain, TComponentDomain>(appDomain);
        }
    }
}

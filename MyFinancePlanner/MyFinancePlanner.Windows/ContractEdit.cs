﻿using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;
using MyFinancePlanner.LedgerEntries.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyFinancePlanner.Windows
{
    public partial class ContractEdit : Form
    {
        private ILedgerEntriesService service = Factory.CreateObject<ILedgerEntriesService>();

        public ContractEdit()
        {
            InitializeComponent();

            this.Load += ContractEdit_Load;
        }

        void ContractEdit_Load(object sender, EventArgs e)
        {
            
        }

        public Contract Contract { get; private set; }

        public void Init(Contract contract)
        {
            this.Contract = contract;
            propertyGrid1.SelectedObject = this.Contract;

            
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                //service.SaveContract(this.Contract);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}

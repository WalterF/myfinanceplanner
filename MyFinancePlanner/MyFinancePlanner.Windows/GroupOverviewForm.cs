﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;
using MyFinancePlanner.Windows.Utils.Extensions;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.Windows
{
    public partial class GroupOverviewForm : Form
    {
        private ILedgerEntriesService service = Factory.CreateObject<ILedgerEntriesService>();

        public GroupOverviewForm()
        {
            InitializeComponent();

            this.Load += GroupOverviewForm_Load;
        }

        void GroupOverviewForm_Load(object sender, EventArgs e)
        {
            try
            {
                InitGrid();
                LoadData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadData()
        {
            /*
            var binding = new BindingSource();

            foreach (var item in service.GetAllGroups())
            {
                binding.Add(item);
            }
            */
            dataGridView1.DataSource = service.GetAllGroups();
        }

        private void InitGrid()
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.AutoSize = true;

            dataGridView1.AddTextBoxColumn("GroupName", "Title");
            dataGridView1.AddEnumColumn<GroupTypeEnum>("GroupType", "Type");

        }
    }
}

﻿using System;
using System.Windows.Forms;

namespace MyFinancePlanner.Windows.Utils.Extensions
{
    public static class DataGridViewExtensions
    {
        public static DataGridViewTextBoxColumn AddTextBoxColumn(this DataGridView gridview, string bindToProperty, string headerText)
        {
            var column = new DataGridViewTextBoxColumn()
            {
                HeaderText = headerText,
                Name = bindToProperty,
                DataPropertyName = bindToProperty
            };
            gridview.Columns.Add(column);

            return column;
        }
        public static DataGridViewComboBoxColumn AddEnumColumn<T>(this DataGridView gridview, string bindToProperty, string headerText) where T : struct
        {
            var column = new DataGridViewComboBoxColumn()
            {
                DataSource = Enum.GetValues(typeof(T)),
                HeaderText = headerText,
                Name = bindToProperty,
                DataPropertyName = bindToProperty
            };
            gridview.Columns.Add(column);

            return column;
        }
    }
}

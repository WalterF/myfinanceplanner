﻿using AutoMapper;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.Windows.StartUp
{
    public class ContractProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Contract, Domain.Contract>()
                .ReverseMap();
        }
    }
}

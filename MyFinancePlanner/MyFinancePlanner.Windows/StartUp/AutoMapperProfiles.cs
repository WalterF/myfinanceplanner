﻿namespace MyFinancePlanner.Windows.StartUp
{
    public class AutoMapperProfiles
    {
        public static void SetUp()
        {
            AutoMapper.Mapper.AddProfile<ContractProfile>();
        }
    }
}

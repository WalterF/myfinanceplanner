﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyFinancePlanner.FinancialScheduler.Domain;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;
using MyFinancePlanner.Web.Models;

namespace MyFinancePlanner.Web.Process
{
    public class PaymentsDetail : ProcessBase
    {
        public IEnumerable<Group> Groups { get; set; }
        public IEnumerable<IGroup> LedgerGroups { get; set; }
        public Payments Payment { get; private set; }
        public Id FinancialYearId { get; private set; }
        public Contract SelectedContract { get; set; }

        public void Do(Id id = null)
        {
            Payment = new Payments();
            Groups = Factory.CreateObject<GetGroups>().Do();
            LedgerGroups = Factory.CreateObject<GetContracts>().Do();
            FinancialYearId = this.financialYearId;

            if (id != null)
            {
                SelectedContract = (from g in this.Groups
                    from c in g.Contracts
                    where c.Id.Equals(id)
                    select c).FirstOrDefault();
            }
        }
    }
}

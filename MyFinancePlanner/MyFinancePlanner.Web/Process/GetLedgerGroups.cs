﻿using System.Collections.Generic;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;

namespace MyFinancePlanner.Web.Process
{
    public class GetLedgerGroups
    {
        public IEnumerable<IGroup> Do()
        {
            var service = Factory.CreateObject<ILedgerEntriesService>();
            return service.GetAllGroups();
        }
    }
}

﻿using MyFinancePlanner.FinancialScheduler.Service;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Core.ObjectValues;

namespace MyFinancePlanner.Web.Process
{
    class RemovePayment : ProcessBase
    {
        public void Do(Id contractId)
        {
            Factory.CreateObject<IFinancialSchedulerService>().RemovePayment(this.financialYearId, contractId);
        }
    }
}

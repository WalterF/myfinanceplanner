﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyFinancePlanner.FinancialScheduler.Service;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Web.Models;

namespace MyFinancePlanner.Web.Process
{
    public class StartHomepage : ProcessBase
    {
        
        private IFinancialSchedulerService service;
        public List<Payments> Payments { get; private set; }
        public Balance Balance { get; private set; }

        public StartHomepage()
        {
            service = Factory.CreateObject<IFinancialSchedulerService>();
            Payments = new List<Payments>();
        }
        public void Do()
        {
            var year = service.GetPayments(financialYearId);

            var contracts = (from x in year.Groups
                from a in x.Contracts 
                where a.ExpectedDate <= DateTime.Now && !a.ProcessingDate.HasValue 
                orderby a.ExpectedDate
                select new Payments() {ContractName=a.ContractName, ContractId=a.Id, 
                    Cost=a.Cost, 
                    ExpectedDate=a.ExpectedDate, 
                    InterestDate=a.InterestDate, ProcessingDate=a.ProcessingDate,
                    GroupId=x.Id, FinancialYearId=year.Id}).ToList();

            Payments = contracts;
            Balance = new Balance()
            {
                CurrentBalance = year.GetCurrentBalance(),
                EstimateBalance = year.GetEstimatedBalance()
            };
        }
    }
}

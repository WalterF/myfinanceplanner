﻿using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.FinancialScheduler.Domain;
using MyFinancePlanner.FinancialScheduler.Service;

namespace MyFinancePlanner.Web.Process
{
    class GetCurrentFinancialYear
    {
        public FinancialYear Do()
        {
            return Factory.CreateObject<IFinancialSchedulerService>().GetCurrent();
        }
    }
}

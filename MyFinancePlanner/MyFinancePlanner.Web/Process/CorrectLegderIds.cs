﻿using System.Linq;
using MyFinancePlanner.FinancialScheduler.Service;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;

namespace MyFinancePlanner.Web.Process
{
    public class CorrectLegderIds : ProcessBase
    {
        public void Do()
        {
            var ledgerGroups = Factory.CreateObject<ILedgerEntriesService>();
            var schedulerService = Factory.CreateObject<IFinancialSchedulerService>();
            var financialYear = schedulerService.Get(this.financialYearId);

            foreach (var legderGroup in ledgerGroups.GetAllGroups())
            {
                var group = financialYear.Groups.First(x => x.GroupName == legderGroup.GroupName);
                group.LedgerEntryGroupGuid = legderGroup.Id;

                foreach (var ledgerContract in legderGroup.Contracts)
                {
                    var contractList = group.Contracts.Where(x => x.ContractName == ledgerContract.ContractName);

                    foreach (var contract in contractList)
                    {
                        contract.LedgerContractGuid = ledgerContract.Id;
                    }
                }
            }
            //schedulerService.Save(financialYear);

        }
    }
}

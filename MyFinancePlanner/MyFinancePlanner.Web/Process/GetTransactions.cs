﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyFinancePlanner.FinancialScheduler.Domain;
using MyFinancePlanner.FinancialScheduler.Service;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Web.Models;

namespace MyFinancePlanner.Web.Process
{
    public class GetTransactions : ProcessBase
    {
        public FinancialYear FinancialYear { get; private set; }
        public List<Transaction> Transactions { get; private set; }
    
        public void Do()
        {
            this.FinancialYear = Factory.CreateObject<IFinancialSchedulerService>().Get(this.financialYearId);
            this.Transactions = (from g in this.FinancialYear.Groups
                from c in g.Contracts
                where c.ProcessingDate.HasValue 
                orderby c.ProcessingDate descending
                select new Transaction()
                {
                    ContractId=c.Id, ContractName=c.ContractName, Cost = c.Cost, 
                    InterestDate = c.InterestDate, ProcessingDate=(DateTime)c.ProcessingDate.Value,
                    GroupId = g.Id,
                    GroupName = g.GroupName, 
                    FinancialYearId=this.FinancialYear.Id
                }).ToList();
        }
    }
}

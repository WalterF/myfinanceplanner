﻿using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Web.Domain;

namespace MyFinancePlanner.Web.Process
{
    class AuthenticateUser
    {
        public bool Do(string username, string password)
        {
            if (username == "walter" && password == "walter12")
            {
                var user = new User { Name = "Walter", Username=username };

                var currentFinancialYear = Factory.CreateObject<GetCurrentFinancialYear>().Do();
                if (currentFinancialYear != null)
                    user.CurrentFinancialYear = (Guid)currentFinancialYear.Id.Value;

                // Create the authentication ticket with custom user data.
                var serializer = new JavaScriptSerializer();
                string userData = serializer.Serialize(user);

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                user.Username,
                DateTime.Now,
                DateTime.Now.AddDays(30),
                true,
                userData,
                FormsAuthentication.FormsCookiePath);

                string encTicket = FormsAuthentication.Encrypt(ticket);

                HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));

                return true;
            }
            return false;
        }
    }
}

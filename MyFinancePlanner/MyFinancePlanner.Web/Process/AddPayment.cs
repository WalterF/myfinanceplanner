﻿using MyFinancePlanner.FinancialScheduler.Service;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Web.Models;

namespace MyFinancePlanner.Web.Process
{
    class AddPayment
    {
        public void Do(Payments payment)
        {
            Factory.CreateObject<IFinancialSchedulerService>()
                .AddPayment(payment.FinancialYearId, payment.GroupId, payment.ContractName, payment.Cost, payment.ProcessingDate.Value, payment.InterestDate, payment.ContractGuid);
        }
    }
}

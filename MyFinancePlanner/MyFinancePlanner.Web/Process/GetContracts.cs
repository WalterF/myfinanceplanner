﻿using System.Collections.Generic;
using System.Linq;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;

namespace MyFinancePlanner.Web.Process
{
    class GetContracts
    {
        public IEnumerable<IGroup> Do()
        {
            var service = Factory.CreateObject<ILedgerEntriesService>();
            return service.GetAllGroups();
        }
    }
}

﻿using MyFinancePlanner.FinancialScheduler.Service;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Web.Models;

namespace MyFinancePlanner.Web.Process
{
    class ConfirmPayment
    {
        public void Do(Payments payment)
        {
            Factory.CreateObject<IFinancialSchedulerService>()
                .ConfirmPayment(payment.FinancialYearId, payment.GroupId, payment.ContractId, payment.Cost, payment.ProcessingDate.Value, payment.InterestDate);
        }
    }
}

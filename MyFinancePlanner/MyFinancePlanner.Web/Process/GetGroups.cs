﻿using System.Collections.Generic;
using MyFinancePlanner.FinancialScheduler.Domain;
using MyFinancePlanner.FinancialScheduler.Service;
using MyFinancePlanner.Core.Factory;

namespace MyFinancePlanner.Web.Process
{
    public class GetGroups : ProcessBase
    {
        public IEnumerable<Group> Do()
        {
            return Factory.CreateObject<IFinancialSchedulerService>()
                .GetGroups(this.financialYearId);
        }
    }
}

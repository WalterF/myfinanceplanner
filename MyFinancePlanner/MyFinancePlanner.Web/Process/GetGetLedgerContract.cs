﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;

namespace MyFinancePlanner.Web.Process
{
    public class GetGetLedgerContract
    {
        public void Do(Id contractId)
        {
            var groups = Factory.CreateObject<GetContracts>().Do();
            this.Contract = (from g in groups
                from c in g.Contracts
                where c.Id.Equals(contractId)
                select c).FirstOrDefault();

            this.Group = groups.FirstOrDefault(x => x.Id.Equals(this.Contract.GroupId));
        }

        public IGroup Group { get; private set; }
        public IContract Contract { get; private set; }
    }
}

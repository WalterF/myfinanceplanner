﻿using System.Web;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.Web.Domain;

namespace MyFinancePlanner.Web.Process
{
    public abstract class ProcessBase
    {
        //protected Id financialYearId = HttpContext.Current.User.Identity
        public Id financialYearId {
            get
            {
                var user = ((MyPrincipal)(HttpContext.Current.User)).User;
                return new Id( user.CurrentFinancialYear);
            }
        }
    }
}

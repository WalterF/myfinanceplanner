﻿using System;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using MyFinancePlanner.Core.ObjectValues;

namespace MyFinancePlanner.Web.Domain
{
    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public Guid CurrentFinancialYear { get; set; }
    }
    public class MyPrincipal : IPrincipal
    {
        public MyPrincipal(IIdentity identity)
        {
            Identity = identity;
        }

        public IIdentity Identity
        {
            get;
            private set;
        }

        public User User { get; set; }

        public bool IsInRole(string role)
        {
            return true;
        }
    }
    //public class MyMembershipProvider : MembershipProvider
    //{
    //    public override bool ValidateUser(string username, string password)
    //    {
    //        // Check if this is a valid user.
    //        User user = UserManager.AuthenticateUser(username, password);
    //        if (user != null)
    //        {
    //            // Store the user temporarily in the context for this request.
    //            HttpContext.Current.Items.Add("User", user);

    //            return true;
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    public override MembershipUser GetUser(string username, bool userIsOnline)
    //    {
    //        if (UserManager.User != null)
    //        {
    //            return new MembershipUser("MyMembershipProvider", username,
    //                       UserManager.User.Id, UserManager.User.Username, null,
    //                       null, true, false, DateTime.MinValue, DateTime.MinValue,
    //                       DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);
    //        }
    //        else
    //        {
    //            return null;
    //        }
    //    }

    //    public override string ApplicationName
    //    {
    //        get
    //        {
    //            throw new NotImplementedException();
    //        }
    //        set
    //        {
    //            throw new NotImplementedException();
    //        }
    //    }

    //    public override bool ChangePassword(string username, string oldPassword, string newPassword)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override bool DeleteUser(string username, bool deleteAllRelatedData)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override bool EnablePasswordReset
    //    {
    //        get { throw new NotImplementedException(); }
    //    }

    //    public override bool EnablePasswordRetrieval
    //    {
    //        get { throw new NotImplementedException(); }
    //    }

    //    public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override int GetNumberOfUsersOnline()
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override string GetPassword(string username, string answer)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override string GetUserNameByEmail(string email)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override int MaxInvalidPasswordAttempts
    //    {
    //        get { throw new NotImplementedException(); }
    //    }

    //    public override int MinRequiredNonAlphanumericCharacters
    //    {
    //        get { throw new NotImplementedException(); }
    //    }

    //    public override int MinRequiredPasswordLength
    //    {
    //        get { throw new NotImplementedException(); }
    //    }

    //    public override int PasswordAttemptWindow
    //    {
    //        get { throw new NotImplementedException(); }
    //    }

    //    public override MembershipPasswordFormat PasswordFormat
    //    {
    //        get { throw new NotImplementedException(); }
    //    }

    //    public override string PasswordStrengthRegularExpression
    //    {
    //        get { throw new NotImplementedException(); }
    //    }

    //    public override bool RequiresQuestionAndAnswer
    //    {
    //        get { throw new NotImplementedException(); }
    //    }

    //    public override bool RequiresUniqueEmail
    //    {
    //        get { throw new NotImplementedException(); }
    //    }

    //    public override string ResetPassword(string username, string answer)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override bool UnlockUser(string userName)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public override void UpdateUser(MembershipUser user)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}

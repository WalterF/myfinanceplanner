﻿namespace MyFinancePlanner.Web.Models
{
    public class Balance
    {
        public decimal CurrentBalance { get; set; }
        public decimal EstimateBalance { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace MyFinancePlanner.Web.Models
{
    public class Login
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        public bool SaveCreadentials { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using MyFinancePlanner.Core.ObjectValues;

namespace MyFinancePlanner.Web.Models
{
    public class Payments
    {
        public Id ContractId { get; set; }
        [Required, Display(Name = "Contract name")]
        public string ContractName { get; set; }
        [Required]
        public decimal Cost { get; set; }
        public DateTime ExpectedDate { get; set; }
        [Required, Display(Name = "Interest date")]
        public DateTime? InterestDate { get; set; }
        [Required, Display(Name = "Processing date")]
        public DateTime? ProcessingDate { get; set; }
        public Id FinancialYearId { get; set; }
        public Id GroupId { get; set; }
        public Id ContractGuid { get; set; }
    }
}

﻿
namespace MyFinancePlanner.Web.App_Start
{
    public class ComponentConfig
    {
        public static void Setup()
        {
            FinancialScheduler.StartUp.ObjectResolver.SetUp();
            LedgerEntries.StartUp.ObjectResolver.SetUp();
        }
    }
}

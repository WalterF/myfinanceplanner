﻿moment.lang('en', {
    week: { dow: 1 }
});
$(function () {
    $('.datetimepicker').datetimepicker({
        format: 'DD-MM-YYYY',
        calendarWeeks: true,
        showTodayButton: true
    });
});

$(function() {
    $('#SelectContract').change(function () {
        var value = $("#SelectContract option:selected").val();
        if (value !== "") {
            $("#ContractName").val($("#SelectContract option:selected").text());
            $("#ContractGuid").val(value);
        } else {
            $("#ContractName").val("");
            $("#ContractGuid").val("");
        }
        
    });
});
﻿using System;
using System.Globalization;
using System.Web.Mvc;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.Web.Models;
using MyFinancePlanner.Web.Process;

namespace MyFinancePlanner.Web.Controllers
{
    public class PaymentController : Controller
    {
        //
        // GET: /Payment/

        public ActionResult Index()
        {
            var usecase = Factory.CreateObject<GetTransactions>();
            usecase.Do();
            return View(usecase);
        }

        //
        // GET: /Payment/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Payment/Create

        public ActionResult Create()
        {
            var usecase = Factory.CreateObject<PaymentsDetail>();
            usecase.Do();

            return View(usecase);
        }

        //
        // POST: /Payment/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                var payment = Factory.CreateObject<Payments>();
                payment.ContractName = collection["ContractName"];
                payment.FinancialYearId = new Id(collection["FinancialYearId"]);
                payment.GroupId = new Id(collection["GroupId"]);
                payment.ContractId = new Id();
                payment.Cost = decimal.Parse(collection["cost"]);
                payment.ProcessingDate = DateTime.Parse(collection["ProcessingDate"], new CultureInfo("nl-NL"));
                payment.InterestDate = !string.IsNullOrEmpty(collection["InterestDate"]) ? DateTime.Parse(collection["InterestDate"], new CultureInfo("nl-NL")) as DateTime? : null as DateTime?;
                payment.ContractGuid = !string.IsNullOrEmpty(collection["ContractGuid"]) ? new Id(new Guid(collection["ContractGuid"])) : new Id(Guid.Empty);

                Factory.CreateObject<AddPayment>().Do(payment);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Payment/Edit/5

        public ActionResult Edit(Guid id)
        {
            var usecase = Factory.CreateObject<PaymentsDetail>();
            usecase.Do(new Id(id));

            return View(usecase);
        }

        //
        // POST: /Payment/Edit/5
        //public ActionResult Edit(int id, FormCollection collection)
        [HttpPost]
        public ActionResult Edit(Guid id, FormCollection collection)
         {
            try
            {
                var action = collection["button"];
                if (action == "delete")
                {
                    Factory.CreateObject<RemovePayment>().Do(new Id(id));    
                }
                else
                {
                    var payment = Factory.CreateObject<Payments>();
                    payment.FinancialYearId = new Id(collection["FinancialYearId"]);
                    payment.GroupId = new Id(collection["GroupId"]);
                    payment.ContractId = new Id(id);
                    payment.Cost = decimal.Parse(collection["cost"]);
                    payment.ProcessingDate = DateTime.Parse(collection["ProcessingDate"], new CultureInfo("nl-NL"));
                    payment.InterestDate = !string.IsNullOrEmpty(collection["InterestDate"]) ? DateTime.Parse(collection["InterestDate"], new CultureInfo("nl-NL")) as DateTime? : null as DateTime?;

                    Factory.CreateObject<ConfirmPayment>().Do(payment);    
                }
                
                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Payment/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Payment/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

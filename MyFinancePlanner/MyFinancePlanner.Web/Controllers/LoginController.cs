﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.Web.Domain;
using MyFinancePlanner.Web.Models;
using MyFinancePlanner.Web.Process;

namespace MyFinancePlanner.Web.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Index()
        {
            var login = new Login();
            return View(login);
        }
       
        [HttpPost]
        public ActionResult Index(Login login)
        {
            try
            {
                string status = "The username or password provided is incorrect.";
                if (ModelState.IsValid)
                {
                    if (Factory.CreateObject<AuthenticateUser>().Do(login.UserName, login.Password))
                    {
                        return RedirectToAction("Index", "Home");    
                    }
                    
                }
                
                return View(login);
            }
            catch
            {
                return View(login);
            }
        }
    }
}

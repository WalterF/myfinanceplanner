﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFinancePlanner.LedgerEntries.Domain.Models;
using MyFinancePlanner.Windows.Domain.Mapper;
using MyFinancePlanner.Windows.StartUp;

namespace MyFinancePlanner.Windows.Test.Domain.Mapper
{
    [TestClass]
    public class ContractMapperTest
    {
        [TestInitialize]
        public void Init()
        {
            AutoMapperProfiles.SetUp();
        }
        [TestMethod, TestCategory("Mapper")]
        public void TestContractMapper()
        {
            var model = new Windows.Domain.Contract();
            model.ContractName = "test";

            var mapper = new DomainMapper<Windows.Domain.Contract, Contract>();
            var domain = mapper.MapToComponentDomain(model);

            Assert.AreEqual(model.ContractName, domain.ContractName);
        }
    }
}

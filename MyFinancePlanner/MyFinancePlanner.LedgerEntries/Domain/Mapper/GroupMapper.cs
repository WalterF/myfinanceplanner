﻿using MyFinancePlanner.LedgerEntries.Data;
using MyFinancePlanner.LedgerEntries.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinancePlanner.LedgerEntries.Domain.Mapper
{
    internal class GroupMapper
    {
        public static GroupData MapToData(Group group)
        {
            return AutoMapper.Mapper.Map<Group, GroupData>(group);
        }
        
        public static Group MapToModel(GroupData groupData)
        {
            return AutoMapper.Mapper.Map<GroupData, Group>(groupData);
        }
    }
}

﻿namespace MyFinancePlanner.LedgerEntries.Domain.Mapper
{
    internal static class IDomainToDomain<TIDomain, TDomain> where TDomain : TIDomain
    {
        public static TDomain ToDomain(TIDomain abastact)
        {
            return AutoMapper.Mapper.Map<TIDomain, TDomain>(abastact);
        }

        public static TIDomain FromDomain(TDomain domain)
        {
            return AutoMapper.Mapper.Map<TIDomain, TDomain>(domain);
        }
    }
}

﻿using System;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.Core.Base;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;

namespace MyFinancePlanner.LedgerEntries.Domain.Models
{
    public class Contract : Model, IContract
    {
        public string ContractName { get; set; }
        public DateTime ExpectedProcessingDate { get; set; }
        public DateTime StartOn { get; set; }
        public DateTime? ExpiredOn { get; set; }
        public RepeatEnum Period { get; set; }
        public decimal Cost { get; set; }
        public Id GroupId { get; set; }
        public string Memo { get; set; }
    }
}

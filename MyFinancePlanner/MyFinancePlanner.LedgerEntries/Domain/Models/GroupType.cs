﻿namespace MyFinancePlanner.LedgerEntries.Domain.Models
{
    public enum GroupTypeEnum
    {
        Income = 1,
        Cost = 2
    }
}

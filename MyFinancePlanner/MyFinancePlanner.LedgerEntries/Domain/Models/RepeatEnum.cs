﻿namespace MyFinancePlanner.LedgerEntries.Domain.Models
{
    public enum RepeatEnum
    {
        None = 0,
        Month =1, 
        Week = 2, 
        Year = 3,
    }
}

﻿using System;
using MyFinancePlanner.Core.Base;
using System.Collections.ObjectModel;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;
using MyFinancePlanner.LedgerEntries.Domain.Factory;

namespace MyFinancePlanner.LedgerEntries.Domain.Models
{
    public class Group : Model, IGroup
    {
        public Group() : base()
        {
            this.Contracts = new Collection<IContract>();
        }
        public string GroupName { get; set; }
        public GroupTypeEnum GroupType { get; set; }
        public Collection<IContract> Contracts { get; private set; }

        public void AddContract(string contractName, decimal cost, DateTime expectedProcessingDate,
            DateTime startDate, DateTime? endDate = null, RepeatEnum period = RepeatEnum.Month, int cycle = 1)
        {
            var contract = ContractFactory.Create(contractName, cost, expectedProcessingDate, startDate, endDate, period, cycle, this.Id);
            this.Contracts.Add(contract);
        }
    }
}

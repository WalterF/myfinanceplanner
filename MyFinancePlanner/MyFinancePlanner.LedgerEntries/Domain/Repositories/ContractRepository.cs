﻿using MyFinancePlanner.LedgerEntries.Data.Gateways;
using MyFinancePlanner.LedgerEntries.Domain.Mapper;
using MyFinancePlanner.LedgerEntries.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace MyFinancePlanner.LedgerEntries.Domain.Repositories
{
    internal class ContractRepository
    {
        private readonly GroupGateway gateway;

        public ContractRepository(GroupGateway gateway)
        {
            this.gateway = gateway;
        }

        //public Contract Save(Contract contract)
        //{
        //    var data = ContractMapper.MapToData(contract);

        //    return ContractMapper.MapToModel(gateway.Save(data));
        //}

        public IEnumerable<Contract> GetAll()
        {
            var groupDatas = gateway.GetAll();
            var contractData = (from obj in groupDatas
                from c in obj.Contracts
                select c).ToList();

            var lstModel = new List<Contract>();
            contractData.ForEach(d => lstModel.Add(ContractMapper.MapToModel(d)));

            return lstModel;
        }
    }
}

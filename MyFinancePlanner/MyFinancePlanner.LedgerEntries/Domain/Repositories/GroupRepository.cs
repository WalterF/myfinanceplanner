﻿using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.LedgerEntries.Data;
using MyFinancePlanner.LedgerEntries.Data.Gateways;
using MyFinancePlanner.LedgerEntries.Domain.Mapper;
using MyFinancePlanner.LedgerEntries.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyFinancePlanner.LedgerEntries.Domain.Repositories
{
    internal class GroupRepository
    {
        private readonly GroupGateway gateway;

        public GroupRepository(GroupGateway gateway)
        {
            this.gateway = gateway;
        }

        public Group Save(Group group)
        {
            var groupData = GroupMapper.MapToData(group);

            groupData = gateway.Save(groupData);

            return GroupMapper.MapToModel(groupData);
        }

        internal IEnumerable<Group> GetAll()
        {
            var lstData = gateway.GetAll();
            var lstModel = new List<Group>(lstData.Count);

            lstData.ForEach(d => lstModel.Add(GroupMapper.MapToModel(d)));

            return lstModel;
        }

        internal bool Delete(Group group)
        {
            return gateway.Delete(group.Id);
        }
    }
}

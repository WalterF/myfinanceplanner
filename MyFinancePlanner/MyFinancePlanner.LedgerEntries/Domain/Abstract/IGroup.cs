﻿using System.Collections.ObjectModel;
using MyFinancePlanner.Core.Base;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.LedgerEntries.Domain.Abstract
{
    public interface IGroup : IModel
    {
        string GroupName { get; set; }
        GroupTypeEnum GroupType { get; set; }
        Collection<IContract> Contracts { get; }
    }
}
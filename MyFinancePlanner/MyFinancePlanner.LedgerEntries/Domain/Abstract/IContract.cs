﻿using System;
using MyFinancePlanner.Core.Base;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.LedgerEntries.Domain.Abstract
{
    public interface IContract : IModel
    {
        string ContractName { get; set; }
        DateTime ExpectedProcessingDate { get; set; }
        DateTime StartOn { get; set; }
        DateTime? ExpiredOn { get; set; }
        RepeatEnum Period { get; set; }
        decimal Cost { get; set; }
        Id GroupId { get; set; }
    }
}
﻿using MyFinancePlanner.LedgerEntries.Domain.Models;
using System.Collections.Generic;

namespace MyFinancePlanner.LedgerEntries.Domain.Abstract
{
    public interface ILedgerEntriesService
    {
        IGroup SaveGroup(IGroup group);
        bool DeleteGroup(IGroup group);
        IEnumerable<IGroup> GetAllGroups();

        //Contract SaveContract(Contract group);
        //bool DeleteContract(Contract group);
        IEnumerable<Contract> GetAllContracts();
    }
}

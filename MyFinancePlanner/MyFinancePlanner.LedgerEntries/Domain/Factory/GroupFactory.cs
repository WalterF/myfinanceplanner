﻿using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.LedgerEntries.Domain.Factory
{
    internal static class GroupFactory
    {
        public static Group Create(string groupName, GroupTypeEnum groupType)
        {
            var group = new Group();
            group.Id = new Id();
            group.GroupName = groupName;
            group.GroupType = groupType;
            
            return group;
        }
    }
}

﻿using System.Diagnostics;
using AutoMapper;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.LedgerEntries.Data;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;
using MyFinancePlanner.LedgerEntries.Domain.Models;

namespace MyFinancePlanner.LedgerEntries.StartUp
{
    internal class GroupProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Group, GroupData>()
                .ForMember(s => s._Id, o => o.MapFrom(d => d.Id))
                .ForMember(s => s.GroupName, o => o.MapFrom(d => d.GroupName))
                .ForMember(s => s.GroupType, o => o.MapFrom(d => d.GroupType))
                .ForMember(s => s.Contracts, o => o.MapFrom(d => d.Contracts))
                .AfterMap((c, d) => Debug.WriteLine("Group  Model to Data"))
                .ReverseMap().ForMember(s => s.Id, o => o.MapFrom(d => new Id(d._Id)))
                .AfterMap((c, d) => Debug.WriteLine("Group data to Model"))
                ;

            Mapper.CreateMap<IGroup, Group>();
        }
    }
}

﻿using AutoMapper;
using MyFinancePlanner.Core.ObjectValues;
using MyFinancePlanner.LedgerEntries.Data;
using MyFinancePlanner.LedgerEntries.Domain.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;

namespace MyFinancePlanner.LedgerEntries.StartUp
{
    internal class ContractProfile : Profile
    {
        protected override void Configure()
        {
            AutoMapper.Mapper.CreateMap<Contract, ContractData>()
                .ForMember(s => s._Id, o => o.MapFrom(d => d.Id))
                .ForMember(s => s.GroupGuid, o => o.MapFrom(d => d.GroupId))
                .ForMember(s => s.Period, o => o.MapFrom(d => d.Period))
                .AfterMap((c, d) => Debug.WriteLine("Contract  Model to Data"))
                .ReverseMap()
                .ForMember(s => s.Id, o => o.MapFrom(d => new Id(d._Id)))
                .ForMember(s => s.GroupId, o => o.MapFrom(d => new Id(d.GroupGuid)))
                .AfterMap((c, d) => Debug.WriteLine("Contract Data to Model"))
                ;

            AutoMapper.Mapper.CreateMap<IContract, ContractData>()
                .ForMember(s => s._Id, o => o.MapFrom(d => d.Id))
                .ForMember(s => s.GroupGuid, o => o.MapFrom(d => d.GroupId))
                .ForMember(s => s.Period, o => o.MapFrom(d => d.Period))
                .AfterMap((c, d) => Debug.WriteLine("Contract  Model to Data"))
                .ReverseMap()
                .ForMember(s => s.Id, o => o.MapFrom(d => new Id(d._Id)))
                .ForMember(s => s.GroupId, o => o.MapFrom(d => new Id(d.GroupGuid)))
                .AfterMap((c, d) => Debug.WriteLine("Contract Data to Model"))
                ;
        }
        
    }
}

﻿using AutoMapper;
using MyFinancePlanner.Core.Factory;
using MyFinancePlanner.LedgerEntries.Domain.Abstract;
using MyFinancePlanner.LedgerEntries.Service;

namespace MyFinancePlanner.LedgerEntries.StartUp
{
    public class ObjectResolver
    {
        public static void SetUp()
        {
            Factory.Register<ILedgerEntriesService, LedgerEntriesService>();

            AutoMapper.Mapper.AddProfile<GroupProfile>();
            AutoMapper.Mapper.AddProfile<ContractProfile>();
        }
    }
}

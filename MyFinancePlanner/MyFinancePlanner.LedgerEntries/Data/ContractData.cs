﻿using MyFinancePlanner.Core.Data;
using MyFinancePlanner.LedgerEntries.Domain.Models;
using System;
using MyFinancePlanner.MongoDb.Data;

namespace MyFinancePlanner.LedgerEntries.Data
{
    internal class ContractData : DocumentBase
    {
        
        public string ContractName { get; set; }
        public DateTime ExpectedProcessingDate { get; set; }
        public DateTime StartOn { get; set; }
        public DateTime? ExpiredOn { get; set; }
        public RepeatEnum Period { get; set; }
        public decimal Cost { get; set; }
        public Guid GroupGuid { get; set; }
    }
}

﻿using System.Collections.ObjectModel;
using MyFinancePlanner.Core.Data;
using MyFinancePlanner.MongoDb.Data;

namespace MyFinancePlanner.LedgerEntries.Data
{
    internal class GroupData : DocumentBase
    {
        public string GroupName { get; set; }
        public int GroupType { get; set; }
        public Collection<ContractData> Contracts { get; set; }
    }
}

﻿using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using MyFinancePlanner.Core.Data.Gateway;
using MyFinancePlanner.MongoDb.Data;

namespace MyFinancePlanner.LedgerEntries.Data.Gateways
{
    internal class GroupGateway
    {
        public string CollectionName { get { return "Group"; } }

        public GroupData Save(GroupData obj)
        {
            var context = new MongoDataContext<GroupData>();
            var collection = context.DataContext.GetCollection<GroupData>(CollectionName);
            var result = collection.Save<GroupData>(obj);
            
            return obj;
        }

        public List<GroupData> GetAll()
        {
            var context = new MongoDataContext<GroupData>();
            var collection = context.DataContext.GetCollection<GroupData>(CollectionName);

            var result = collection.FindAllAs<GroupData>();
            
            return result.ToList<GroupData>();
        }

        internal bool Delete(Guid id)
        {
            var context = new MongoDataContext<GroupData>();
            var collection = context.DataContext.GetCollection<GroupData>(CollectionName);

            var query = Query.EQ("_id", id);
            var result = collection.Remove(query);

            return result.DocumentsAffected > 0;
        }
    }
}

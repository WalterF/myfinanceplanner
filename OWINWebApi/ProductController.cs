﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace OWINWebApi
{
    public class ProductController : ApiController 
    {
        private readonly List<Product> products = new List<Product>()
            {
                new Product() {Id = 1, Name = "Schoen"},
                new Product() {Id = 2, Name = "Reus"}
            };


        public List<Product> Get()
        {
            return products;
        }

        public Product Get(long id)
        {
            return products.FirstOrDefault(x => x.Id == id);
        }
    }
}

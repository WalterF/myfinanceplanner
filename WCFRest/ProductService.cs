﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCFRest
{
    [ServiceContract]
    public interface IProductService
    {
        [OperationContract]
        List<Product> GetAll();
        [OperationContract]
        Product Get(string id);
        [OperationContract]
        Product GetSub(string id, string id2);
    }
    class ProductService : IProductService
    {
        private readonly List<Product> products;
        public ProductService()
        {
            products = new List<Product>()
            {
                new Product() {Id = 1, Name = "Schoen"},
                new Product() {Id = 2, Name = "Reus"}
            };
        }
        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    UriTemplate = "/data/{id}")]
        public Product Get(string id)
        {
            long i = long.Parse(id);
            return products.FirstOrDefault(x => x.Id == i);
        }

        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    UriTemplate = "/data/{id}/{id2}")]
        public Product GetSub(string id, string id2)
        {
            long i = long.Parse(id);
            return products.FirstOrDefault(x => x.Id == i);
        }

        [WebInvoke(Method = "GET",
                    ResponseFormat = WebMessageFormat.Json,
                    UriTemplate = "/data")]
        public List<Product> GetAll()
        {
            return products;
        }
    }
}

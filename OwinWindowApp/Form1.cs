﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OwinWindowApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            this.Load += Form1_Load;
        }

        void Form1_Load(object sender, EventArgs e)
        {
            var apiPath = @"E:\BitBucket\MyFinancePlanner\OWINWebApi\bin\Debug\OWINWebApi.exe";

            ProcessStartInfo foo = new ProcessStartInfo(apiPath);
            foo.CreateNoWindow = false;
            //foo.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(foo);

            GetJsonData();
        }

        private void GetJsonData()
        {
            string baseAddress = "http://localhost:9000/";

            HttpClient client = new HttpClient();

            var response = client.GetAsync(baseAddress + "product").Result;

            Console.WriteLine(response);
            var lst = JsonHelper.Deserialize<List<Product>>(response.Content.ReadAsStringAsync().Result);

            gridview.DataSource = lst;
        }
    }
}
